/// Checks for chosen character
if(instance_exists(oPortrait)){
	// Selects nearest cell and snaps
	var inst = instance_nearest(x, y, oPortrait);
	if(place_meeting(x, y, inst)) {
		oPlayer1.sprite_index = inst.sprite;
		if(select){
			instance_create_layer(oPlayer1.x, oPlayer1.y, "Players", inst.character);
			room_goto_next();
		}
	}
}