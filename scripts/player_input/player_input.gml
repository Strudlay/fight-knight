// Movement Controls
if(room == rmCharacterSelection){
left = keyboard_check_pressed(ord("A"));
right = keyboard_check_pressed(ord("D"));
up = keyboard_check_pressed(ord("W"));
down = keyboard_check_pressed(ord("S"));
}
if(room == rmStage){
left = keyboard_check(ord("A"));
right = keyboard_check(ord("D"));
up = keyboard_check_pressed(ord("W"));
down = keyboard_check_pressed(ord("S"));
}
// Select
select = keyboard_check_pressed(vk_enter);
// Attack
attack = mouse_check_button(mb_left);

// Reset for debug
if (keyboard_check_pressed(ord("R")))
{
	game_restart();	
}