/// Calculate player movement

// determine player's direction
hsp = (right - left) * spd;
// Apply gravity
vsp += grav;
// Set facing direction
if hsp != 0 facing = sign(hsp);