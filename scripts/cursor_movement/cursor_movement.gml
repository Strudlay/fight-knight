/// Cursor movement

// Horizontal movement
hsp = (right - left) * width;
// Vertical movement
vsp = (down - up) * height;
// Grab nearest cell and snap
if(instance_exists(oPortrait)){
	// Restricts the cursor on x axis
	if((x + hsp) > maxX || (x + hsp) < minX){
		hsp = 0;	
	}
	// Restricts the cursor on y axis
	if((y + vsp) > maxY || (y + vsp) < minY){
		vsp = 0;	
	}
	// Selects nearest cell and snaps
	var inst = instance_nearest(x, y, oPortrait);
	move_snap(inst.x, inst.y);
}
// Adjusts position
x+=hsp;
y+=vsp;