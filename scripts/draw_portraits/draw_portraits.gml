/// Draws portraits to screen
for(var j = 0; j < rows; j++){
	for(var i = 0; i < cols; i++){
		var inst = instance_create_layer(xPos + (i * width), yPos + (j * height), "Portraits", oPortrait);
		inst.sprite_index = portrait_sprites[i + (cols * j)];
		inst.image_speed = 0;
		inst.character = character_objects[i + (cols * j)];
		inst.sprite = idle_sprites[i + (cols * j)];
	}
}