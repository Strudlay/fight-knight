/// @desc

// Inherit the parent event
event_inherited();
// states array
states_array[states.IDLE] = player_idle_state;
states_array[states.WALK] = player_walk_state;
//states_array[states.ATTACK] = player_attack_state;
// sprites array
sprites_array[states.IDLE] = knightIdle;
sprites_array[states.WALK] = knightWalk;
//sprites_array[states.ATTACK] = s_player_attack;
// mask array
mask_array[states.IDLE] = knightIdle;
mask_array[states.WALK] = knightWalk;
//mask_array[states.ATTACK] = s_player_idle;