{
    "id": "8b53736b-5f37-4871-9466-adba32e85e29",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKnight",
    "eventList": [
        {
            "id": "00057c5e-262d-4dab-b932-85214e56b85f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8b53736b-5f37-4871-9466-adba32e85e29"
        },
        {
            "id": "f2198146-7c4b-47bd-bb94-fcb7ac3841e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8b53736b-5f37-4871-9466-adba32e85e29"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b02f1cb1-82d3-4938-ae36-288f806264db",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
    "visible": true
}