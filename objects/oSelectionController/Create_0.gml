/// @desc

// Enum for portraits
enum characters {
	KNIGHT, 
	MAGE,
	ROBOT,
	GUNNER,
	SKELETON,
	ARCHER,
	CHIEF, 
	WOLF
}
// Portrait sprites array
portrait_sprites[characters.KNIGHT] = knightPortrait;
portrait_sprites[characters.MAGE] = magePortrait;
portrait_sprites[characters.ROBOT] = robotPortrait;
portrait_sprites[characters.GUNNER] = gunnerPortrait;
portrait_sprites[characters.SKELETON] = skeletonPortrait;
portrait_sprites[characters.ARCHER] = archerPortrait;
portrait_sprites[characters.CHIEF] = chiefPortrait;
portrait_sprites[characters.WOLF] = wolfPortrait;
// Idle sprites array
idle_sprites[characters.KNIGHT] = knightIdle;
idle_sprites[characters.MAGE] = mageIdle;
idle_sprites[characters.ROBOT] = robotIdle;
idle_sprites[characters.GUNNER] = gunnerIdle;
idle_sprites[characters.SKELETON] = skeletonIdle;
idle_sprites[characters.ARCHER] = archerIdle;
idle_sprites[characters.CHIEF] = chiefIdle;
idle_sprites[characters.WOLF] = wolfIdle;
// Character object array
character_objects[characters.KNIGHT] = oKnight;
character_objects[characters.MAGE] = oMage;
character_objects[characters.ROBOT] = robotIdle;
character_objects[characters.GUNNER] = gunnerIdle;
character_objects[characters.SKELETON] = skeletonIdle;
character_objects[characters.ARCHER] = archerIdle;
character_objects[characters.CHIEF] = chiefIdle;
character_objects[characters.WOLF] = wolfIdle;
// Set positions
xPos = oFrame.x + 104;
yPos = oFrame.y + 80;
// Set rows and columns
rows = 2;
cols = 4;
height = 128;
width = 160;
// Draw portraits
draw_portraits();