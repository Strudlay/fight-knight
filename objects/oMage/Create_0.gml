/// @desc

// Inherit the parent event
event_inherited();

// states array
states_array[states.IDLE] = player_idle_state;
states_array[states.WALK] = player_walk_state;
//states_array[states.ATTACK] = player_attack_state;
// sprites array
sprites_array[states.IDLE] = mageIdle;
sprites_array[states.WALK] = mageWalk;
//sprites_array[states.ATTACK] = s_player_attack;
// mask array
mask_array[states.IDLE] = mageIdle;
mask_array[states.WALK] = mageWalk;
//mask_array[states.ATTACK] = s_player_idle;