{
    "id": "7553bf8d-859c-4f01-821f-a9c7e1619e1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMage",
    "eventList": [
        {
            "id": "55fa16d8-70c2-47eb-bb85-12d5e2498513",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7553bf8d-859c-4f01-821f-a9c7e1619e1a"
        },
        {
            "id": "885bebfc-222c-4643-9601-a02398728bbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7553bf8d-859c-4f01-821f-a9c7e1619e1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b02f1cb1-82d3-4938-ae36-288f806264db",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
    "visible": true
}