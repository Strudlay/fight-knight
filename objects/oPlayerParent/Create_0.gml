/// @desc
left = 0;
right = 0;
attack = 0;
hsp = 0;
vsp = 0;
spd = 7;

facing = 1;
grav = 0.1;

// States
enum states {
	IDLE,
	WALK, 
	ATTACK
}
// Set default state
state = states.IDLE;