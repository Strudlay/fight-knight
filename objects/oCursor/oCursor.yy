{
    "id": "e49e2cf2-397f-4db3-a9a4-5f81a2aa632b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCursor",
    "eventList": [
        {
            "id": "d7f904b2-0326-4ac9-a973-61936d5c1b92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e49e2cf2-397f-4db3-a9a4-5f81a2aa632b"
        },
        {
            "id": "f00a9fdb-1d01-4d4d-97da-34c48115ab5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e49e2cf2-397f-4db3-a9a4-5f81a2aa632b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c5d7ba80-d358-4e7a-9199-3c88d46ee605",
    "visible": true
}