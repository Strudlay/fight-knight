/// @desc

// Movement controls
left = 0;
right = 0;
up = 0;
down = 0;
// Select controls
select = 0;
// Movement
hsp = 0;
vsp = 0;
// Cell dimensions
height = 128;
width = 155;
// Image speed
image_speed = 0;
// Ranges for x and y
minX = 250;
maxX = 760;
minY = 260;
maxY = 422;