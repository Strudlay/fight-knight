{
    "id": "aee1119b-04a4-4dbd-b024-f03252d5bb6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wolfPortrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 3,
    "bbox_right": 151,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c267d6b-511f-41e7-8c0f-1161f558b001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aee1119b-04a4-4dbd-b024-f03252d5bb6a",
            "compositeImage": {
                "id": "cde3d59d-5644-45ec-aaee-906a7ea596e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c267d6b-511f-41e7-8c0f-1161f558b001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccff075e-da64-4e9c-9874-5b925ce2f3af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c267d6b-511f-41e7-8c0f-1161f558b001",
                    "LayerId": "634df765-5f1f-4ee5-b607-9d869a6cfe08"
                }
            ]
        },
        {
            "id": "7fcf289b-36de-4111-830a-b866fc012cce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aee1119b-04a4-4dbd-b024-f03252d5bb6a",
            "compositeImage": {
                "id": "e754006c-aa75-4edc-9ced-368f4649e4de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fcf289b-36de-4111-830a-b866fc012cce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75240927-c818-4e14-b2ee-1f77bde84de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fcf289b-36de-4111-830a-b866fc012cce",
                    "LayerId": "634df765-5f1f-4ee5-b607-9d869a6cfe08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "634df765-5f1f-4ee5-b607-9d869a6cfe08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aee1119b-04a4-4dbd-b024-f03252d5bb6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 155,
    "xorig": 105,
    "yorig": 58
}