{
    "id": "d6feab2e-c000-4b86-b34a-3618f856810e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gunnerAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 92,
    "bbox_right": 383,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b824ddd0-7fb5-41f6-81cb-db7355f8f823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "c8c993ab-9a93-4e74-b042-afffefb6f8bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b824ddd0-7fb5-41f6-81cb-db7355f8f823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "612324c5-7a21-455a-94da-b19b410650f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b824ddd0-7fb5-41f6-81cb-db7355f8f823",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "475c1725-cedb-423a-b116-27d39680578b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "deae994e-f5f3-45fd-8976-234e24164b4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "475c1725-cedb-423a-b116-27d39680578b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c293af-c056-4d6a-802a-def9e36abb93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "475c1725-cedb-423a-b116-27d39680578b",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "2edfc837-22a9-4284-b11c-8dea712ce0a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "01c2accf-680a-4653-8924-b294e8965803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2edfc837-22a9-4284-b11c-8dea712ce0a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1a0ba03-d0f2-42b3-9240-75a1b26c3afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2edfc837-22a9-4284-b11c-8dea712ce0a3",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "c9065844-cc7a-4c40-8cb7-127fc5b0d7c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "dd16b912-9b5a-4b82-9cb1-21e0da8f6d6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9065844-cc7a-4c40-8cb7-127fc5b0d7c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30a1185d-bc38-4131-aa8f-66d9ef470e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9065844-cc7a-4c40-8cb7-127fc5b0d7c5",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "3d4c5b96-1d7d-40f9-83b2-cedfe991411c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "d8eea52d-cac1-404d-b84a-f981d4954557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d4c5b96-1d7d-40f9-83b2-cedfe991411c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f7b19b6-bb65-4594-94ce-b4344c68ecc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4c5b96-1d7d-40f9-83b2-cedfe991411c",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "26374c46-1cd0-44be-8dbc-b279e33e35b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "ce0b9e67-46de-4cf9-b449-19ab2208d085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26374c46-1cd0-44be-8dbc-b279e33e35b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69104f1d-9084-4761-b9b3-c01f71b1893a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26374c46-1cd0-44be-8dbc-b279e33e35b5",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "4b24abcc-1822-45ef-a0a3-cf8c0a98ea66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "d9ea3fdd-72ec-4866-8c4f-77be18f68869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b24abcc-1822-45ef-a0a3-cf8c0a98ea66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b697f7fd-bb2d-4f74-81b6-920e641862e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b24abcc-1822-45ef-a0a3-cf8c0a98ea66",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "f4a9789e-126b-4b49-a119-053ec3585d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "9eb4aedf-c203-4a20-b8f2-d975cbb5dcd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a9789e-126b-4b49-a119-053ec3585d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3722cc64-8aad-44ba-a36e-c8d8c6c68739",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a9789e-126b-4b49-a119-053ec3585d56",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "22474a59-3cd9-4f47-ad81-75fe651ef917",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "771ce52d-f61e-4a5a-9782-7bc64a216ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22474a59-3cd9-4f47-ad81-75fe651ef917",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16826c36-b7be-4516-8fd0-36f76e779028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22474a59-3cd9-4f47-ad81-75fe651ef917",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "d241f157-42c3-4a90-90e2-0c66932b6987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "65f70f5b-9dcf-45e2-afba-b54f5e1c77f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d241f157-42c3-4a90-90e2-0c66932b6987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f40f9675-f3f8-4686-a2c0-22070eb04cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d241f157-42c3-4a90-90e2-0c66932b6987",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "89f635eb-ccf4-465b-b564-ffca410b6afd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "644fec58-4f9b-4f3c-a02b-151c9cfaa472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89f635eb-ccf4-465b-b564-ffca410b6afd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3436d46-741b-4769-aedb-8ba473a0edf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89f635eb-ccf4-465b-b564-ffca410b6afd",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "f5037e69-ee4b-4436-8294-1e000883e1bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "e3858e2d-d62c-46fd-b07c-0b8a0bf63b41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5037e69-ee4b-4436-8294-1e000883e1bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd348326-8703-4fb6-8a4c-78ace6d74fbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5037e69-ee4b-4436-8294-1e000883e1bc",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "5f04001b-3374-4b81-b0c3-221061ff5d9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "e0e43149-6df9-434e-8410-7856f7d3990e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f04001b-3374-4b81-b0c3-221061ff5d9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aca4858-c5e1-46c6-ac26-f9ee93753807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f04001b-3374-4b81-b0c3-221061ff5d9f",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "34409c15-5c2d-47a0-979b-133bf76cc6a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "01e60e97-0138-44b4-aecd-d2bf8b95df01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34409c15-5c2d-47a0-979b-133bf76cc6a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42efd1d6-742f-499c-bc0d-1d25e68628ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34409c15-5c2d-47a0-979b-133bf76cc6a0",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "2635ee00-26c9-4b7c-a6ca-971396c5a70b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "3f2c488f-3221-4128-aae0-92eea9d8f6d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2635ee00-26c9-4b7c-a6ca-971396c5a70b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9106a31e-8658-48db-8769-f69e0c932bf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2635ee00-26c9-4b7c-a6ca-971396c5a70b",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "5e9c0277-b4ca-481d-905b-630d9974614f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "78f111e2-2f78-48f2-ba67-3d6c9f47e7be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e9c0277-b4ca-481d-905b-630d9974614f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d05346b0-9876-40d0-9fd2-aec945a15cce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e9c0277-b4ca-481d-905b-630d9974614f",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "263de343-ed10-4250-9407-7bbaf19a69f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "31199718-c57a-467c-ac00-1107166ce5d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "263de343-ed10-4250-9407-7bbaf19a69f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa822224-78b6-4c3c-99d5-059798d00505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "263de343-ed10-4250-9407-7bbaf19a69f9",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "30ba6040-1970-4233-ae38-7492947d8b6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "6c6b020a-cabb-4e7f-9f57-addd1be80d83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30ba6040-1970-4233-ae38-7492947d8b6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de8ee73f-e5cd-4c4a-b7b6-8cb2304484c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30ba6040-1970-4233-ae38-7492947d8b6f",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "9d5f34f5-16c7-4d59-a6a2-d634a2b3ece4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "2fec3334-14c5-4d78-b24f-691deb317fb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d5f34f5-16c7-4d59-a6a2-d634a2b3ece4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b1cf2b6-87db-425f-87c9-c363264079cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d5f34f5-16c7-4d59-a6a2-d634a2b3ece4",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "c810256d-d783-4c61-81ab-9a3643c1f415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "5172e77f-bd64-480f-ab2f-5d8d86c46762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c810256d-d783-4c61-81ab-9a3643c1f415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f956f6f-29fe-4926-a15c-7356d859eac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c810256d-d783-4c61-81ab-9a3643c1f415",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "d2cafea6-f8f7-48ea-9336-354a71c22840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "222eb168-c508-48c5-be70-fae42e2e3dd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2cafea6-f8f7-48ea-9336-354a71c22840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f799b3b3-34a4-4639-93dd-d9625cb1b7f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2cafea6-f8f7-48ea-9336-354a71c22840",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "075a77e8-ecfe-46e5-8bb6-099195727fbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "bba6657a-85f5-48bc-b10d-6d89bf6b1194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "075a77e8-ecfe-46e5-8bb6-099195727fbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7577b217-babb-4832-bec4-08171e647d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "075a77e8-ecfe-46e5-8bb6-099195727fbc",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        },
        {
            "id": "35518100-779c-4a94-941b-5b09b24069dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "compositeImage": {
                "id": "2397312a-dd8b-4e66-b525-f2a72492b3bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35518100-779c-4a94-941b-5b09b24069dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de11254d-a69b-427d-bd76-55958f6a687a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35518100-779c-4a94-941b-5b09b24069dd",
                    "LayerId": "d67f2250-6cb5-467b-981a-6f02d552a8f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "d67f2250-6cb5-467b-981a-6f02d552a8f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6feab2e-c000-4b86-b34a-3618f856810e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 192,
    "yorig": 96
}