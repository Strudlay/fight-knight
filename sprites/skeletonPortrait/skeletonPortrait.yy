{
    "id": "a605ea64-2408-4b43-8561-ea43a0b41a7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeletonPortrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 0,
    "bbox_right": 154,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cbcc8f0-646c-4496-ab97-126e803d4d7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a605ea64-2408-4b43-8561-ea43a0b41a7e",
            "compositeImage": {
                "id": "61defa48-2446-4a75-892b-b67c9689f839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cbcc8f0-646c-4496-ab97-126e803d4d7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c2aa534-b38d-4abc-987b-a1dc5372f7e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cbcc8f0-646c-4496-ab97-126e803d4d7f",
                    "LayerId": "c86e1986-fc95-4ff1-9bea-9680712bb00c"
                }
            ]
        },
        {
            "id": "6cebdac4-46ad-43f3-94e4-297291b55e24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a605ea64-2408-4b43-8561-ea43a0b41a7e",
            "compositeImage": {
                "id": "e0c6ec75-1074-48c7-8463-74c4010d8639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cebdac4-46ad-43f3-94e4-297291b55e24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b67a410-739e-4a0f-800b-64549b5d5e02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cebdac4-46ad-43f3-94e4-297291b55e24",
                    "LayerId": "c86e1986-fc95-4ff1-9bea-9680712bb00c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "c86e1986-fc95-4ff1-9bea-9680712bb00c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a605ea64-2408-4b43-8561-ea43a0b41a7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 155,
    "xorig": 77,
    "yorig": 58
}