{
    "id": "e54e7502-b6d6-451a-a013-706c7fe48d94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "chiefPortrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 0,
    "bbox_right": 154,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6da66dd-9814-4e5c-8c48-c604fb00eeb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e54e7502-b6d6-451a-a013-706c7fe48d94",
            "compositeImage": {
                "id": "bd4278b2-83fd-483b-b95a-076d81f56000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6da66dd-9814-4e5c-8c48-c604fb00eeb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf0a944a-3aea-4d04-92e8-e4ef37c7d835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6da66dd-9814-4e5c-8c48-c604fb00eeb8",
                    "LayerId": "337e57fa-12b2-4400-a218-e6a23b5b8cea"
                }
            ]
        },
        {
            "id": "d5d25167-ed05-4fee-bef2-51d915209838",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e54e7502-b6d6-451a-a013-706c7fe48d94",
            "compositeImage": {
                "id": "316dd910-4b66-49c4-8a2e-90a722378150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5d25167-ed05-4fee-bef2-51d915209838",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e45a53a-1ad7-4684-95e6-b6dfabeea14c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5d25167-ed05-4fee-bef2-51d915209838",
                    "LayerId": "337e57fa-12b2-4400-a218-e6a23b5b8cea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "337e57fa-12b2-4400-a218-e6a23b5b8cea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e54e7502-b6d6-451a-a013-706c7fe48d94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 155,
    "xorig": 77,
    "yorig": 58
}