{
    "id": "a3043040-aa3d-44b6-add6-ab4267ea9ebc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "chiefAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 271,
    "bbox_left": 16,
    "bbox_right": 223,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32c84db8-8d12-4968-847f-b62f4b7b29e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3043040-aa3d-44b6-add6-ab4267ea9ebc",
            "compositeImage": {
                "id": "ec381309-180a-4d9f-b2f7-272adae0420b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c84db8-8d12-4968-847f-b62f4b7b29e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b84da2aa-bfa2-41c8-bb08-070c20132be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c84db8-8d12-4968-847f-b62f4b7b29e2",
                    "LayerId": "ce1aa087-816e-4409-86c8-df27e57aebba"
                }
            ]
        },
        {
            "id": "1b072ead-c92c-49dc-937c-4f91b1cf34f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3043040-aa3d-44b6-add6-ab4267ea9ebc",
            "compositeImage": {
                "id": "fffc6b70-0476-4f71-aa40-1dcd3fd8c969",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b072ead-c92c-49dc-937c-4f91b1cf34f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f8d3132-6181-4c53-b844-acf2a127856b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b072ead-c92c-49dc-937c-4f91b1cf34f7",
                    "LayerId": "ce1aa087-816e-4409-86c8-df27e57aebba"
                }
            ]
        },
        {
            "id": "31c170c9-9453-4cd6-a2f9-dd71084214da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3043040-aa3d-44b6-add6-ab4267ea9ebc",
            "compositeImage": {
                "id": "972cc598-4b03-4c0f-b386-85e414300c76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31c170c9-9453-4cd6-a2f9-dd71084214da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91dfbf95-39b3-4ec6-ae95-3ab24279730e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31c170c9-9453-4cd6-a2f9-dd71084214da",
                    "LayerId": "ce1aa087-816e-4409-86c8-df27e57aebba"
                }
            ]
        },
        {
            "id": "31a5a04f-a0d8-426c-bab9-0dc5a24d7d4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3043040-aa3d-44b6-add6-ab4267ea9ebc",
            "compositeImage": {
                "id": "9480a0b0-a5bd-419f-a157-465da6a69528",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a5a04f-a0d8-426c-bab9-0dc5a24d7d4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee0fdc64-7143-409e-ad79-474b10b6ccc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a5a04f-a0d8-426c-bab9-0dc5a24d7d4f",
                    "LayerId": "ce1aa087-816e-4409-86c8-df27e57aebba"
                }
            ]
        },
        {
            "id": "5b87bbac-77b9-44c5-b4f3-ae7c3ff7b8a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3043040-aa3d-44b6-add6-ab4267ea9ebc",
            "compositeImage": {
                "id": "149c8ae0-b673-4800-8df7-3a3dd9e8cff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b87bbac-77b9-44c5-b4f3-ae7c3ff7b8a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abcd0ab2-bfdd-48ad-8beb-becc70347169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b87bbac-77b9-44c5-b4f3-ae7c3ff7b8a8",
                    "LayerId": "ce1aa087-816e-4409-86c8-df27e57aebba"
                }
            ]
        },
        {
            "id": "99c545c4-2599-4571-ac5e-c3768d50820b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3043040-aa3d-44b6-add6-ab4267ea9ebc",
            "compositeImage": {
                "id": "4e14d341-dbd6-4da2-8590-856ff935f1ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99c545c4-2599-4571-ac5e-c3768d50820b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5dd5781-e3a9-46e3-ae1c-512fd71496ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99c545c4-2599-4571-ac5e-c3768d50820b",
                    "LayerId": "ce1aa087-816e-4409-86c8-df27e57aebba"
                }
            ]
        },
        {
            "id": "dd947652-f65b-4a4f-b202-c7df3ce0e08b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3043040-aa3d-44b6-add6-ab4267ea9ebc",
            "compositeImage": {
                "id": "76a2cebb-c71a-4665-99f6-165819e1293e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd947652-f65b-4a4f-b202-c7df3ce0e08b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46e0bd7f-8ccc-4f7c-9d4b-6db4c324f26d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd947652-f65b-4a4f-b202-c7df3ce0e08b",
                    "LayerId": "ce1aa087-816e-4409-86c8-df27e57aebba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "ce1aa087-816e-4409-86c8-df27e57aebba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3043040-aa3d-44b6-add6-ab4267ea9ebc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 144,
    "yorig": 144
}