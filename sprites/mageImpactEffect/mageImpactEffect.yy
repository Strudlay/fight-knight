{
    "id": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mageImpactEffect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "caa6f36f-e077-4ac3-b142-8b391fb923ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "671134b9-20e9-4979-beeb-666b9157701b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caa6f36f-e077-4ac3-b142-8b391fb923ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55dbf847-1025-47fb-90fb-9773753a1b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caa6f36f-e077-4ac3-b142-8b391fb923ea",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        },
        {
            "id": "bbd3f3e3-d01b-4ada-8065-3e099121d980",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "cc0e57e3-c2ea-4fa9-84cb-2a605ec018cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd3f3e3-d01b-4ada-8065-3e099121d980",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cc2b50d-41e6-41df-acc1-5e5e788fe0ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd3f3e3-d01b-4ada-8065-3e099121d980",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        },
        {
            "id": "0b9cb49d-c6f1-41de-8daa-802875b2c298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "bd144055-ee83-4787-b040-9a6318b1b11d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b9cb49d-c6f1-41de-8daa-802875b2c298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27eed3e1-2d3c-49a2-986a-91a5c5f6f8cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b9cb49d-c6f1-41de-8daa-802875b2c298",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        },
        {
            "id": "718e1acb-3093-4ada-ad18-d01b42e027a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "3254efce-7883-4bb1-9dbd-0a4404700ad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "718e1acb-3093-4ada-ad18-d01b42e027a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f7c09b9-0c3c-46a1-a002-28a2e3f94913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "718e1acb-3093-4ada-ad18-d01b42e027a6",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        },
        {
            "id": "6a792c7c-b7f0-48f1-8466-f98104e38d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "fccfac4e-9b52-4771-a506-2f072ed41ba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a792c7c-b7f0-48f1-8466-f98104e38d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35427619-22cc-42b7-bd49-0540e04fe72d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a792c7c-b7f0-48f1-8466-f98104e38d68",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        },
        {
            "id": "3f619d50-e8fa-4716-afaf-08374fea6b7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "b23351b5-1c91-43b3-84a7-d6f5d6b94601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f619d50-e8fa-4716-afaf-08374fea6b7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37a89339-333c-4ad6-84ed-7e091ebdaaed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f619d50-e8fa-4716-afaf-08374fea6b7f",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        },
        {
            "id": "dedbd72a-5b83-4707-9077-99f57e2b26e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "14b32fba-6bfa-45d9-a614-84dc9eabd0c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dedbd72a-5b83-4707-9077-99f57e2b26e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42bf3adb-d254-47a2-9521-f28fc8864747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dedbd72a-5b83-4707-9077-99f57e2b26e5",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        },
        {
            "id": "834e5802-0dc5-4cab-99e4-1272fa61f2a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "4e24a67f-b005-4a78-8ea7-9a4fdab22098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "834e5802-0dc5-4cab-99e4-1272fa61f2a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84db41e7-1915-4fef-bd04-ef6aaeb433d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "834e5802-0dc5-4cab-99e4-1272fa61f2a6",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        },
        {
            "id": "9663d5d0-a98d-436f-8ccf-10afc87a27be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "9005db78-e3d0-40f5-9eab-3aefe34b1360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9663d5d0-a98d-436f-8ccf-10afc87a27be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "670c443a-f89e-4f8a-b280-364d7e9f4ec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9663d5d0-a98d-436f-8ccf-10afc87a27be",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        },
        {
            "id": "fdc409f4-4541-4990-8502-cfe259e8396d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "aad30afc-389d-4f72-b627-8ab3e9cfe6be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdc409f4-4541-4990-8502-cfe259e8396d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44eb232-a8ae-4368-87b4-321ff6c17bf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdc409f4-4541-4990-8502-cfe259e8396d",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        },
        {
            "id": "f788ea6d-c82d-444f-8de8-e1bfaf03a6bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "compositeImage": {
                "id": "30f30c37-5067-4933-94a5-0d6588a113dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f788ea6d-c82d-444f-8de8-e1bfaf03a6bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fd095f1-0d64-41b7-bb85-4daa48f47b31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f788ea6d-c82d-444f-8de8-e1bfaf03a6bf",
                    "LayerId": "8ddea01b-450a-4f04-afa0-64ec209a387b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "8ddea01b-450a-4f04-afa0-64ec209a387b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a00f531-9bed-4761-aae9-6e6625b4fd89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}