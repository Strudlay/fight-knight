{
    "id": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robotWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 4,
    "bbox_right": 191,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c37434a2-34ea-41c3-b7f3-f9c678aeea08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
            "compositeImage": {
                "id": "2c9f18d3-e561-4671-bd2b-1f4dc640ab77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37434a2-34ea-41c3-b7f3-f9c678aeea08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b069f477-78f3-4c09-abd8-fc3f27abb074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37434a2-34ea-41c3-b7f3-f9c678aeea08",
                    "LayerId": "70eef0c3-70ee-4193-8311-dc518f705c08"
                }
            ]
        },
        {
            "id": "7b891ece-3761-46d5-ae6a-bd7cb10e1429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
            "compositeImage": {
                "id": "8cf2b05f-3fc6-4703-a184-f2102b14838d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b891ece-3761-46d5-ae6a-bd7cb10e1429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a935c503-98d8-4d5a-b6a0-3bfe6c75ed73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b891ece-3761-46d5-ae6a-bd7cb10e1429",
                    "LayerId": "70eef0c3-70ee-4193-8311-dc518f705c08"
                }
            ]
        },
        {
            "id": "09720f1f-bdba-49d2-a5e2-71bfe23ddf6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
            "compositeImage": {
                "id": "dc6e301f-5455-4bfa-84de-276443038acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09720f1f-bdba-49d2-a5e2-71bfe23ddf6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d7b6e3-d450-4566-807d-a332be3d2261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09720f1f-bdba-49d2-a5e2-71bfe23ddf6c",
                    "LayerId": "70eef0c3-70ee-4193-8311-dc518f705c08"
                }
            ]
        },
        {
            "id": "009df97a-4b0b-4141-bebd-78ab69af1585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
            "compositeImage": {
                "id": "62f3a197-2fc1-4629-bd64-4e0bf9dd94ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "009df97a-4b0b-4141-bebd-78ab69af1585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ff12d71-aa26-4423-9fbf-1ffd7815b717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "009df97a-4b0b-4141-bebd-78ab69af1585",
                    "LayerId": "70eef0c3-70ee-4193-8311-dc518f705c08"
                }
            ]
        },
        {
            "id": "bde3733b-e8b9-42ea-aa36-3d9b514319ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
            "compositeImage": {
                "id": "9f7f7fb4-7d00-4492-96eb-b8c250714b61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde3733b-e8b9-42ea-aa36-3d9b514319ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eca5774-c403-444b-a3ca-03ee780f9cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde3733b-e8b9-42ea-aa36-3d9b514319ec",
                    "LayerId": "70eef0c3-70ee-4193-8311-dc518f705c08"
                }
            ]
        },
        {
            "id": "90578bac-b751-4674-ba8b-4f9aee98a6fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
            "compositeImage": {
                "id": "ae94c62e-a6c9-47bd-b1f4-ba5dfb10ebb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90578bac-b751-4674-ba8b-4f9aee98a6fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74987eea-4053-410e-a777-8b520ba93802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90578bac-b751-4674-ba8b-4f9aee98a6fc",
                    "LayerId": "70eef0c3-70ee-4193-8311-dc518f705c08"
                }
            ]
        },
        {
            "id": "47c8955f-3b00-472e-8e48-cf6a408bb1c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
            "compositeImage": {
                "id": "4ef45eed-ef01-4d35-b83e-b338599b4af6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47c8955f-3b00-472e-8e48-cf6a408bb1c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc1d6868-dbe7-4522-887f-c86a8bf1a5f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47c8955f-3b00-472e-8e48-cf6a408bb1c1",
                    "LayerId": "70eef0c3-70ee-4193-8311-dc518f705c08"
                }
            ]
        },
        {
            "id": "894501d3-9a10-4493-8fca-5a2de5f4045d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
            "compositeImage": {
                "id": "8f08152d-3e8e-4929-8199-4ddda696f047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "894501d3-9a10-4493-8fca-5a2de5f4045d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e65dc72-9adf-4e66-9dc2-8d9208a80a31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "894501d3-9a10-4493-8fca-5a2de5f4045d",
                    "LayerId": "70eef0c3-70ee-4193-8311-dc518f705c08"
                }
            ]
        },
        {
            "id": "ee910c84-ab93-4955-9a51-1d82a6c6f3d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
            "compositeImage": {
                "id": "88999183-72d2-41e3-bc77-5625e40bc247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee910c84-ab93-4955-9a51-1d82a6c6f3d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da0b8417-2d6e-4733-a5ad-47404c3eb881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee910c84-ab93-4955-9a51-1d82a6c6f3d4",
                    "LayerId": "70eef0c3-70ee-4193-8311-dc518f705c08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "70eef0c3-70ee-4193-8311-dc518f705c08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "176ffbf3-4b12-40d0-8ddc-e04a6cf5510a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}