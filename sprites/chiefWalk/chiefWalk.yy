{
    "id": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "chiefWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 16,
    "bbox_right": 191,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6708c178-2a69-4efc-b5bf-602cc818753b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "3881cc0b-6c13-4594-b9bd-165e81c46a26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6708c178-2a69-4efc-b5bf-602cc818753b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09924eb7-9085-4031-8d1e-1efd3a97fe78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6708c178-2a69-4efc-b5bf-602cc818753b",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "2c2640a4-6283-4904-aa29-cca884d17430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "2308b947-24a0-4e48-90aa-72a3bd693a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c2640a4-6283-4904-aa29-cca884d17430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1809c492-ea9e-4f6f-849b-271991f9ab3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c2640a4-6283-4904-aa29-cca884d17430",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "4eb4f1be-8461-462d-9c40-f0e4b9077ca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "3752db5f-405e-495d-992a-c251d02b707c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb4f1be-8461-462d-9c40-f0e4b9077ca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae443712-1c0b-4a77-bb60-9597c8c5f944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb4f1be-8461-462d-9c40-f0e4b9077ca7",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "6757c303-08e1-4781-aa3d-bf54e4327083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "309ad43c-86db-4c93-a0ff-ebfeaec5e115",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6757c303-08e1-4781-aa3d-bf54e4327083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c761784a-e382-496f-9180-295fd23f1d88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6757c303-08e1-4781-aa3d-bf54e4327083",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "d49d4d83-acf4-4c37-ab6a-ee28121b8481",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "d8bfebec-d0df-43a5-94be-c5245cbc1c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d49d4d83-acf4-4c37-ab6a-ee28121b8481",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ddd200-c3e2-41fe-b8ea-2c72227b4bec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d49d4d83-acf4-4c37-ab6a-ee28121b8481",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "a39b420e-3cc7-47b0-a2ad-e75aef032961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "eb072aaa-9e6a-4255-a613-788660c959ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a39b420e-3cc7-47b0-a2ad-e75aef032961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30df9c55-23f9-4a90-ab9d-7983c56a15f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a39b420e-3cc7-47b0-a2ad-e75aef032961",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "36982150-9738-4a78-971d-f84ede651c3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "c48a9a98-1216-42c4-acc0-83ff245e9f21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36982150-9738-4a78-971d-f84ede651c3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91010657-b38f-4b49-b51d-782bf6265d5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36982150-9738-4a78-971d-f84ede651c3f",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "8e7cba7f-4226-491a-a483-9c06e9462528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "93289b2a-57ef-4ad2-9fe6-86a9b87c06c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e7cba7f-4226-491a-a483-9c06e9462528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f6342ce-7458-4a6b-8f1e-e3b8633fece1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e7cba7f-4226-491a-a483-9c06e9462528",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "19206738-0d36-4014-920a-9ec309026f88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "7b34b154-def4-4285-bbd3-c86352a2540e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19206738-0d36-4014-920a-9ec309026f88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd490d9-068b-4caa-946f-68b2b65a4491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19206738-0d36-4014-920a-9ec309026f88",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "ff056765-eeb9-4581-acfa-bd8d845fc6dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "2900ee1e-e8ea-4723-96d3-70364ec8a29a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff056765-eeb9-4581-acfa-bd8d845fc6dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea224407-38e1-4aec-9971-ee1309c2da74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff056765-eeb9-4581-acfa-bd8d845fc6dd",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "a9a2fe5e-eb64-4a93-b82a-4ab84d054dbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "25324383-a037-4f86-ab30-faab4230d96e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9a2fe5e-eb64-4a93-b82a-4ab84d054dbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57447c44-4ad4-4d97-85c7-7a50d6a1ccae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9a2fe5e-eb64-4a93-b82a-4ab84d054dbf",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        },
        {
            "id": "e55e6267-8964-4ef2-8d61-d6212f3bf8be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "compositeImage": {
                "id": "9b656809-8617-4890-89ae-307c93b54227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e55e6267-8964-4ef2-8d61-d6212f3bf8be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "604d1eea-0413-4cfe-8746-4dc29e06384f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e55e6267-8964-4ef2-8d61-d6212f3bf8be",
                    "LayerId": "03b26f64-b20d-4f87-8300-db28f42576d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "03b26f64-b20d-4f87-8300-db28f42576d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c1cd0a3-e12a-492f-84a7-46725c5a7d01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}