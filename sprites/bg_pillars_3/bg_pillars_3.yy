{
    "id": "99e652b3-819f-4953-b1f4-9314dc0e9bbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_pillars_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 183,
    "bbox_left": 40,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b72962a-a172-4e7c-901f-39d87b40491b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99e652b3-819f-4953-b1f4-9314dc0e9bbc",
            "compositeImage": {
                "id": "de0b7413-f5fd-4bea-ba8c-08c0ebde664c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b72962a-a172-4e7c-901f-39d87b40491b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "263030ef-a930-4bf4-96b2-f73c6ae1eb3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b72962a-a172-4e7c-901f-39d87b40491b",
                    "LayerId": "b61780d0-b3dc-4a9a-88f4-6c1f9e73d7cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 184,
    "layers": [
        {
            "id": "b61780d0-b3dc-4a9a-88f4-6c1f9e73d7cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99e652b3-819f-4953-b1f4-9314dc0e9bbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}