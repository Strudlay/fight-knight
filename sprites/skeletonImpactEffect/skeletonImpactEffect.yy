{
    "id": "eed49645-13e5-41d2-9fde-e55c38e2fe98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeletonImpactEffect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 4,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22fd3551-d823-45cb-8c0b-03c1c2cbc5bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eed49645-13e5-41d2-9fde-e55c38e2fe98",
            "compositeImage": {
                "id": "504a1dc8-9750-4f8d-ac12-68bc0645e7b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22fd3551-d823-45cb-8c0b-03c1c2cbc5bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5310862d-0bde-467e-9149-fb831a1f1ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22fd3551-d823-45cb-8c0b-03c1c2cbc5bf",
                    "LayerId": "44110df7-f807-47fb-bf22-cd5cc8e0fb33"
                }
            ]
        },
        {
            "id": "fbbb7b67-e554-49e8-adcf-93475e5ab2ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eed49645-13e5-41d2-9fde-e55c38e2fe98",
            "compositeImage": {
                "id": "dec740c1-c791-4d6f-ad68-60f184c1937e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbbb7b67-e554-49e8-adcf-93475e5ab2ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a10499ab-5a89-47df-8c5b-21e8b18eff85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbbb7b67-e554-49e8-adcf-93475e5ab2ad",
                    "LayerId": "44110df7-f807-47fb-bf22-cd5cc8e0fb33"
                }
            ]
        },
        {
            "id": "0b6e7f14-3d4a-4588-a14e-7f49b020d3a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eed49645-13e5-41d2-9fde-e55c38e2fe98",
            "compositeImage": {
                "id": "1887f1e3-0eb1-41cc-b976-86954994121a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b6e7f14-3d4a-4588-a14e-7f49b020d3a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02bc7aa5-70ad-4516-9a52-39bcc0a12428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6e7f14-3d4a-4588-a14e-7f49b020d3a9",
                    "LayerId": "44110df7-f807-47fb-bf22-cd5cc8e0fb33"
                }
            ]
        },
        {
            "id": "5c36c8ea-ad0c-4763-8a16-af3ec48d5079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eed49645-13e5-41d2-9fde-e55c38e2fe98",
            "compositeImage": {
                "id": "99c2b373-63ba-4bf0-97dd-1c6a813e5fe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c36c8ea-ad0c-4763-8a16-af3ec48d5079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e33bd25a-d7e8-43ac-b3a2-bb456c72feaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c36c8ea-ad0c-4763-8a16-af3ec48d5079",
                    "LayerId": "44110df7-f807-47fb-bf22-cd5cc8e0fb33"
                }
            ]
        },
        {
            "id": "7e6af936-4ca9-4f21-b8e3-5de846ec24fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eed49645-13e5-41d2-9fde-e55c38e2fe98",
            "compositeImage": {
                "id": "2b304ee7-dd43-4cd3-abf4-c25afd033dcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e6af936-4ca9-4f21-b8e3-5de846ec24fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11ade090-3c39-4591-9a99-9ab966d688fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e6af936-4ca9-4f21-b8e3-5de846ec24fe",
                    "LayerId": "44110df7-f807-47fb-bf22-cd5cc8e0fb33"
                }
            ]
        },
        {
            "id": "a74cb42f-7eb1-47b6-b953-aba3a62e1b8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eed49645-13e5-41d2-9fde-e55c38e2fe98",
            "compositeImage": {
                "id": "390b1703-8cce-417f-833b-e506de44c07d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a74cb42f-7eb1-47b6-b953-aba3a62e1b8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d5d57ac-1e58-427e-87b4-e490237db244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a74cb42f-7eb1-47b6-b953-aba3a62e1b8d",
                    "LayerId": "44110df7-f807-47fb-bf22-cd5cc8e0fb33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "44110df7-f807-47fb-bf22-cd5cc8e0fb33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eed49645-13e5-41d2-9fde-e55c38e2fe98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}