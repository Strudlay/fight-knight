{
    "id": "7c90337a-456f-4608-b52b-6cbf917033f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "knightWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 171,
    "bbox_left": 16,
    "bbox_right": 191,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22ee9ec6-9d1e-4dc7-a7ef-4aab28a286eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "b83de87b-46ee-4ced-a3f9-1a13c5cd80cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22ee9ec6-9d1e-4dc7-a7ef-4aab28a286eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22555941-444e-48f7-92d8-4fb6318edae5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22ee9ec6-9d1e-4dc7-a7ef-4aab28a286eb",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "08191711-ec3e-4228-a16d-b871a88ffbf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "663d43d3-22de-44ac-8252-b41aadd979c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08191711-ec3e-4228-a16d-b871a88ffbf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4844ba9e-5a20-49d4-997d-3df6d45d9640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08191711-ec3e-4228-a16d-b871a88ffbf8",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "6cac6912-d341-4317-a352-da2f3545ee43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "50037bb7-a5ed-4048-ac79-b1d1fe2fc923",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cac6912-d341-4317-a352-da2f3545ee43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76317eab-61b9-4d85-a120-8518556455a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cac6912-d341-4317-a352-da2f3545ee43",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "82a09f62-dea3-410c-9178-3a0e49914d82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "7d0b2ef1-7f55-4b6f-8ae7-848055c044e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a09f62-dea3-410c-9178-3a0e49914d82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab060d3-be0e-4d80-a26b-43a35b49663b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a09f62-dea3-410c-9178-3a0e49914d82",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "59be797d-c4a7-4596-94e3-16d3225f94fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "99ccf462-202d-4338-9647-a2a42870c751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59be797d-c4a7-4596-94e3-16d3225f94fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe44a03d-579b-4079-9b2d-d43899784f4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59be797d-c4a7-4596-94e3-16d3225f94fb",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "9e670b33-1857-4de2-831d-80083aee1c31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "4378867c-aa64-4332-8fd7-3a4560ddbdeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e670b33-1857-4de2-831d-80083aee1c31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda20e16-fd93-4deb-8092-937b3bc2970b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e670b33-1857-4de2-831d-80083aee1c31",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "41689533-cfe2-424d-897e-04e5d02982d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "b7d1058c-4ec4-4376-bc7f-b34a4a526cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41689533-cfe2-424d-897e-04e5d02982d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72b75e6f-92d0-47be-a6b7-992005bfbd7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41689533-cfe2-424d-897e-04e5d02982d0",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "c5a189a0-0ad2-4936-b5da-a46ee6f2ebcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "e84c08f4-0fe3-42e8-8495-2a5becabc596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a189a0-0ad2-4936-b5da-a46ee6f2ebcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c562108-c14b-4b3a-8a54-6be5d914471f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a189a0-0ad2-4936-b5da-a46ee6f2ebcf",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "3e5a1358-1ef2-4dc9-8342-736e7c0f137f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "6f2588f6-a3a5-411e-b777-c20ac4e8c5af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e5a1358-1ef2-4dc9-8342-736e7c0f137f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d060c4c-9b43-4ee5-bfd2-1c76209718e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e5a1358-1ef2-4dc9-8342-736e7c0f137f",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "dd3ef2a5-e5ce-4111-baf2-3907513558ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "c50609f7-8dbe-4ac2-b670-87cce8add2ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd3ef2a5-e5ce-4111-baf2-3907513558ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a6059f0-3e7e-4ac7-b240-babe860abba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd3ef2a5-e5ce-4111-baf2-3907513558ff",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "c4be3593-0492-40a4-b09c-aef0f0790e45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "78e57ef8-4ca7-49e2-b8be-911a429469be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4be3593-0492-40a4-b09c-aef0f0790e45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c52413aa-b5fb-433a-9135-c9ac8d1c89d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4be3593-0492-40a4-b09c-aef0f0790e45",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        },
        {
            "id": "2fe97240-6ddf-4ae0-9c33-13e791cd9c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "compositeImage": {
                "id": "62cdf0a6-7dee-49d9-8bac-b01b2cac9d08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe97240-6ddf-4ae0-9c33-13e791cd9c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acebd779-c7f7-4e61-942a-5ecc1b6c6b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe97240-6ddf-4ae0-9c33-13e791cd9c8e",
                    "LayerId": "e3ada000-b441-4be3-b675-a0560db01187"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "e3ada000-b441-4be3-b675-a0560db01187",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c90337a-456f-4608-b52b-6cbf917033f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 70,
    "yorig": 87
}