{
    "id": "1efc0e67-491d-47dc-b0e8-33693dccd0e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "knightPortrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 20,
    "bbox_right": 134,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39814379-fc08-47f9-9050-bad79891a336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1efc0e67-491d-47dc-b0e8-33693dccd0e1",
            "compositeImage": {
                "id": "8e0a81e3-fc66-440b-9f76-3030ec657065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39814379-fc08-47f9-9050-bad79891a336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "090bd154-ef75-47c2-96e3-41646bce2e0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39814379-fc08-47f9-9050-bad79891a336",
                    "LayerId": "150af645-c160-46c8-be3e-002345071101"
                }
            ]
        },
        {
            "id": "933517c2-0e28-4bff-9a2e-0754873442dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1efc0e67-491d-47dc-b0e8-33693dccd0e1",
            "compositeImage": {
                "id": "9357d7f1-374c-45c7-a00a-81ce0c2c9665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933517c2-0e28-4bff-9a2e-0754873442dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee2f9d85-129a-48f8-b898-db39df0aa951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933517c2-0e28-4bff-9a2e-0754873442dc",
                    "LayerId": "150af645-c160-46c8-be3e-002345071101"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "150af645-c160-46c8-be3e-002345071101",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1efc0e67-491d-47dc-b0e8-33693dccd0e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 155,
    "xorig": 77,
    "yorig": 58
}