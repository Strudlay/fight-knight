{
    "id": "1b224a57-83c7-430a-833c-5731d46b70b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robotImpactEffect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 235,
    "bbox_left": 16,
    "bbox_right": 231,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88aa2591-840d-4330-829c-314246719c6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b224a57-83c7-430a-833c-5731d46b70b0",
            "compositeImage": {
                "id": "cb3c4a9b-9f73-497b-b8f3-7ae110479aaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88aa2591-840d-4330-829c-314246719c6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c7f79a-7883-48d2-9313-0f157d6fda96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88aa2591-840d-4330-829c-314246719c6b",
                    "LayerId": "5a9b56f8-3a16-414e-8283-47c8c396b060"
                }
            ]
        },
        {
            "id": "b9cb6f4b-ec76-48d2-9af9-ddeeb8a271ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b224a57-83c7-430a-833c-5731d46b70b0",
            "compositeImage": {
                "id": "f057892b-b69a-43a7-b41c-2a448e670aae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9cb6f4b-ec76-48d2-9af9-ddeeb8a271ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "553b420f-6f4c-4edf-be09-59168ef48732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9cb6f4b-ec76-48d2-9af9-ddeeb8a271ec",
                    "LayerId": "5a9b56f8-3a16-414e-8283-47c8c396b060"
                }
            ]
        },
        {
            "id": "132bd06e-839a-4b2e-bf03-c11453c4f98d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b224a57-83c7-430a-833c-5731d46b70b0",
            "compositeImage": {
                "id": "77585485-a61b-410a-939a-6e0bcdff650e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132bd06e-839a-4b2e-bf03-c11453c4f98d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1501eb87-9730-42f9-a9d5-54dd9c759904",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132bd06e-839a-4b2e-bf03-c11453c4f98d",
                    "LayerId": "5a9b56f8-3a16-414e-8283-47c8c396b060"
                }
            ]
        },
        {
            "id": "4e3af0c2-008a-4d6f-87fc-f0ecb1d5f025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b224a57-83c7-430a-833c-5731d46b70b0",
            "compositeImage": {
                "id": "96cfab59-03de-425a-90d8-b0e704f11062",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e3af0c2-008a-4d6f-87fc-f0ecb1d5f025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23409045-d9b9-4688-8ae0-1228b4f1b72c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e3af0c2-008a-4d6f-87fc-f0ecb1d5f025",
                    "LayerId": "5a9b56f8-3a16-414e-8283-47c8c396b060"
                }
            ]
        },
        {
            "id": "00bf57c1-3aa0-4a94-b3ca-1e5d438e9679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b224a57-83c7-430a-833c-5731d46b70b0",
            "compositeImage": {
                "id": "9358b1f0-0222-4d30-a7fc-fcdbb8d1be67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00bf57c1-3aa0-4a94-b3ca-1e5d438e9679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31d7c621-2b49-4fab-a374-b138cbdecd11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00bf57c1-3aa0-4a94-b3ca-1e5d438e9679",
                    "LayerId": "5a9b56f8-3a16-414e-8283-47c8c396b060"
                }
            ]
        },
        {
            "id": "31a6c0a1-1de2-41b9-a30f-502e7f46d2f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b224a57-83c7-430a-833c-5731d46b70b0",
            "compositeImage": {
                "id": "1229c010-37ef-45f5-9096-dc15e5e12e5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a6c0a1-1de2-41b9-a30f-502e7f46d2f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8270bd4-e61b-4c6d-8fb5-fd242ca9d916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a6c0a1-1de2-41b9-a30f-502e7f46d2f7",
                    "LayerId": "5a9b56f8-3a16-414e-8283-47c8c396b060"
                }
            ]
        },
        {
            "id": "614466f8-6aae-432d-b7e4-5fa4f904e7ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b224a57-83c7-430a-833c-5731d46b70b0",
            "compositeImage": {
                "id": "33046a15-0d29-4e87-8775-ffe314428155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "614466f8-6aae-432d-b7e4-5fa4f904e7ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f61dcc2d-35c7-4c61-b021-69e5d11eb6f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "614466f8-6aae-432d-b7e4-5fa4f904e7ef",
                    "LayerId": "5a9b56f8-3a16-414e-8283-47c8c396b060"
                }
            ]
        },
        {
            "id": "c3cd6c11-1dc8-4340-86e3-6565e8946025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b224a57-83c7-430a-833c-5731d46b70b0",
            "compositeImage": {
                "id": "824c4d21-a335-4440-bba5-1bc5498ef703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3cd6c11-1dc8-4340-86e3-6565e8946025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0153c94-d694-4cfd-bec5-84b4bc1785e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3cd6c11-1dc8-4340-86e3-6565e8946025",
                    "LayerId": "5a9b56f8-3a16-414e-8283-47c8c396b060"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "5a9b56f8-3a16-414e-8283-47c8c396b060",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b224a57-83c7-430a-833c-5731d46b70b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 120
}