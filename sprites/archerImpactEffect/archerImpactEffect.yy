{
    "id": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "archerImpactEffect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 20,
    "bbox_right": 179,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38262580-36c5-423d-8886-223156d2ca92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "eb042665-6e81-4acf-aaed-e47876a6a197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38262580-36c5-423d-8886-223156d2ca92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6b4fed0-c57e-4e43-8fdd-8192749f2ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38262580-36c5-423d-8886-223156d2ca92",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "0f469d38-f315-4178-b50c-1c7414b2f3f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "ae155559-8db4-41b1-902b-5fd64c5e9739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f469d38-f315-4178-b50c-1c7414b2f3f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae3eb554-8c40-4256-9f8d-13ea1fe5799a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f469d38-f315-4178-b50c-1c7414b2f3f7",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "4d522969-f493-4b50-a765-69ef88c38c68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "1ac51dd3-c5ff-487d-a410-7af01c954a6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d522969-f493-4b50-a765-69ef88c38c68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843b6b69-c960-4e04-b24f-fa76d3c9af9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d522969-f493-4b50-a765-69ef88c38c68",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "6e207715-681e-44b6-afd2-e20d42fd2dea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "4a722191-494b-4cc4-9383-b489e25e8075",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e207715-681e-44b6-afd2-e20d42fd2dea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0301d26c-1118-4524-975c-9d34f49622c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e207715-681e-44b6-afd2-e20d42fd2dea",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "c288a066-8116-4f08-824b-e87313dd9833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "c2c6cb3f-f7cd-4723-bd61-32af944bff76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c288a066-8116-4f08-824b-e87313dd9833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae4e06d-e172-4681-92e7-74d4439f7262",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c288a066-8116-4f08-824b-e87313dd9833",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "f3daf11a-92fb-4c0f-b66b-78cafebf1ab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "fbc34ee0-6300-4bf0-8f56-ac6530572bf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3daf11a-92fb-4c0f-b66b-78cafebf1ab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3e9e644-faa7-457f-a883-70df461c55c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3daf11a-92fb-4c0f-b66b-78cafebf1ab0",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "8e2c433f-a518-401e-b2ab-c2d3465dadde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "fe0a85ac-cdc0-47e8-925c-c58eebb8859d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e2c433f-a518-401e-b2ab-c2d3465dadde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdf7f831-30a7-48ac-bca2-318d26dd7e89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e2c433f-a518-401e-b2ab-c2d3465dadde",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "f13d3411-59a5-4518-b6f6-12c918aeef8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "6516e4e2-d193-42c3-b5b1-50b10fe3e226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f13d3411-59a5-4518-b6f6-12c918aeef8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a575b36-f7b8-494f-8e63-f80084bb36c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13d3411-59a5-4518-b6f6-12c918aeef8c",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "dc399f56-d794-41d3-9bbe-044657a975dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "c1623b41-e87f-4d07-a85a-57ef8717bc9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc399f56-d794-41d3-9bbe-044657a975dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e63cd8f-3fd2-4d4d-bd4c-dc1a642497a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc399f56-d794-41d3-9bbe-044657a975dc",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "f75bc2e4-1d4e-4250-8034-d54cc10d53bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "e7a912ab-9a6b-44d6-b1a3-52dda6f75953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f75bc2e4-1d4e-4250-8034-d54cc10d53bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b692c8f-863b-4975-8232-52b3d84020cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f75bc2e4-1d4e-4250-8034-d54cc10d53bd",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "2fa0a969-9dfd-4e28-a5bb-d9819777d2d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "d018054b-ba29-49d8-8421-e218fc521368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fa0a969-9dfd-4e28-a5bb-d9819777d2d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5808d1b-dbe9-4565-bbf1-313c221054eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fa0a969-9dfd-4e28-a5bb-d9819777d2d8",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        },
        {
            "id": "2b1249e9-229a-4ea2-b888-0c7dcdfcbce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "compositeImage": {
                "id": "234a1a18-3c38-439c-aa61-633a366f3e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b1249e9-229a-4ea2-b888-0c7dcdfcbce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "549f81c9-c704-48a2-a1cb-dbfa51777d44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b1249e9-229a-4ea2-b888-0c7dcdfcbce3",
                    "LayerId": "3270f22e-dd74-414f-89ee-fa16205baffa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "3270f22e-dd74-414f-89ee-fa16205baffa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d6364ee-5409-42ce-add3-bdeb4b642d66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}