{
    "id": "8ddda783-09dc-445c-b4e3-c9181012f25c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "archerAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 20,
    "bbox_right": 287,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7efc00bc-cfce-4b20-909f-fa867fd34896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "eb98affc-5652-426e-97a8-4c9fcda699e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7efc00bc-cfce-4b20-909f-fa867fd34896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3095f60-7795-4c7e-ab87-a957ed5eefdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7efc00bc-cfce-4b20-909f-fa867fd34896",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "202fb0e7-2a36-4961-a073-f99143c3aa78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "f0f3cb4e-9524-42d5-ba39-12b3dc23b03b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "202fb0e7-2a36-4961-a073-f99143c3aa78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e40bba18-b30c-4066-b1c9-8828b716c942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "202fb0e7-2a36-4961-a073-f99143c3aa78",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "5f941b5b-0fb4-4bea-9395-77ff1f5af85a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "d998ddbf-90be-412a-878a-e5c84ee2e90d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f941b5b-0fb4-4bea-9395-77ff1f5af85a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b445bcb-b65f-449a-8a34-51eb6401e1c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f941b5b-0fb4-4bea-9395-77ff1f5af85a",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "353747be-40ad-4483-bc55-d11ccd5bacf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "adf54ddf-42ca-4584-8b01-d0b2ca020d28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "353747be-40ad-4483-bc55-d11ccd5bacf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2c00d6d-5753-4126-a317-982309a756af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "353747be-40ad-4483-bc55-d11ccd5bacf3",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "d113f407-0b99-4e33-baf2-e3f73e7a70f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "9dc2830f-c8f2-4a2a-9f23-0902f0c1d7a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d113f407-0b99-4e33-baf2-e3f73e7a70f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7145a5d4-0456-4146-b14a-7b1c6c5ea139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d113f407-0b99-4e33-baf2-e3f73e7a70f4",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "f88602db-8e35-4169-afd6-a010c256964e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "e4023ae0-4c9d-4a0d-b7bb-eb6b64cd397f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f88602db-8e35-4169-afd6-a010c256964e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3730907b-a1db-4324-bd64-a7482c4c54b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f88602db-8e35-4169-afd6-a010c256964e",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "c7491b01-cbf3-4c80-996a-8a4c491e2770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "350a2444-66a1-42e1-a69b-6eaf4b914d32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7491b01-cbf3-4c80-996a-8a4c491e2770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32489813-d1a8-4003-8194-520f53b2e77d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7491b01-cbf3-4c80-996a-8a4c491e2770",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "ff74a663-a980-4524-b00e-60c00b3b76df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "e3f9aca7-e1e3-4879-946f-4b5c1d90610f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff74a663-a980-4524-b00e-60c00b3b76df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffcc61bd-9daf-4578-bede-e5a3ce087eac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff74a663-a980-4524-b00e-60c00b3b76df",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "a513dc52-8335-4285-839d-8f6e39c92bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "84a92f16-f4d4-4941-8d75-de39aaea34d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a513dc52-8335-4285-839d-8f6e39c92bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c8a91d1-0abc-42a2-9ad3-7c7229952878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a513dc52-8335-4285-839d-8f6e39c92bd8",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "206b42ba-d651-46a6-a2bf-f694b6206b0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "0795f211-a6fa-497d-8903-125e26ae33df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "206b42ba-d651-46a6-a2bf-f694b6206b0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9246585f-13b7-4a1d-b5f1-a26a86831560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "206b42ba-d651-46a6-a2bf-f694b6206b0a",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "9a819479-db10-48d6-bd07-a23beb895645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "18ee431a-c46d-4cc3-9fbd-1b189d2b2497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a819479-db10-48d6-bd07-a23beb895645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dd340b0-4759-4cec-bed3-8e4a46bfd16d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a819479-db10-48d6-bd07-a23beb895645",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "a64cdb38-38c4-49b5-ab34-5f0181512c16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "4cacebb5-93e8-485f-b1ac-3256dbc3ca46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a64cdb38-38c4-49b5-ab34-5f0181512c16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "521c5d2d-e806-4b6f-a0bc-73e715035b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a64cdb38-38c4-49b5-ab34-5f0181512c16",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "f31d59e8-3eaa-45f2-be70-d6d3c3f2c3b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "ab1b9e8f-e966-46fb-bf2a-eb8846f4b0e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f31d59e8-3eaa-45f2-be70-d6d3c3f2c3b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3850999-2293-4e07-97ba-c9b49d000b90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f31d59e8-3eaa-45f2-be70-d6d3c3f2c3b8",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "8d4ed520-8e32-44c6-b1a0-a182dba07800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "b0e8e742-333e-473a-898a-c4290c233ad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d4ed520-8e32-44c6-b1a0-a182dba07800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abfb12e5-11d1-4041-8496-176877f5fc00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d4ed520-8e32-44c6-b1a0-a182dba07800",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "08fbcdfd-3e44-43f9-bcaf-43fcc03aa8a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "92355138-22e5-4079-ad91-52fcccf52476",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08fbcdfd-3e44-43f9-bcaf-43fcc03aa8a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00aaeff3-8f12-4b56-98f4-a6622dd887c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08fbcdfd-3e44-43f9-bcaf-43fcc03aa8a7",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        },
        {
            "id": "27808c74-3fe5-4c1b-a906-2a5eb8fb4af1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "compositeImage": {
                "id": "f4b8752d-0476-41f8-a6ad-192f49999266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27808c74-3fe5-4c1b-a906-2a5eb8fb4af1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ffd3fbf-5a99-4519-af2f-164d8df631d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27808c74-3fe5-4c1b-a906-2a5eb8fb4af1",
                    "LayerId": "f55a86f5-f95a-436d-b3ad-fae65b428c00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "f55a86f5-f95a-436d-b3ad-fae65b428c00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ddda783-09dc-445c-b4e3-c9181012f25c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 144,
    "yorig": 96
}