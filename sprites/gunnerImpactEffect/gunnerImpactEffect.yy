{
    "id": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gunnerImpactEffect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 183,
    "bbox_left": 0,
    "bbox_right": 171,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ffd8389-dc72-48a9-aaf9-6a81e595a728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "67c6c8b7-e6da-471d-9ed3-aa770390cbf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ffd8389-dc72-48a9-aaf9-6a81e595a728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f3742ed-7dc0-404e-9112-f011b1579fd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ffd8389-dc72-48a9-aaf9-6a81e595a728",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "f1226d60-f0ed-45b8-b469-a7b77d0442e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "553ef40c-734a-48cc-9e6b-a610d83fc062",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1226d60-f0ed-45b8-b469-a7b77d0442e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8658ec30-cef3-4b37-b551-1b626d5f412b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1226d60-f0ed-45b8-b469-a7b77d0442e8",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "f5cefb7a-9cac-418a-967e-779bf2e9c1bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "66a7151f-4911-4e4d-b3bf-4f211b9eca04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5cefb7a-9cac-418a-967e-779bf2e9c1bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feed70e1-b227-42fc-9421-6c8e98970e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5cefb7a-9cac-418a-967e-779bf2e9c1bb",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "be3ea559-1185-4cf5-ac10-b615c015863a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "8db6de99-d930-4eb4-86f5-78bbdc0477b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3ea559-1185-4cf5-ac10-b615c015863a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a942de1b-1966-4600-8a97-113a6ebf3804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3ea559-1185-4cf5-ac10-b615c015863a",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "835834c6-0d6a-47e8-98fe-e5e2e4f4fc5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "dbbf1357-33d5-4448-9673-b503008cd580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "835834c6-0d6a-47e8-98fe-e5e2e4f4fc5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "881ecf90-7e6c-450a-9df0-70d59034a879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "835834c6-0d6a-47e8-98fe-e5e2e4f4fc5b",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "638e50ad-cce1-4c91-8b3f-8e280d91d669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "0c3babf2-94d0-472e-a17b-6a39b3f55ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "638e50ad-cce1-4c91-8b3f-8e280d91d669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44d4698d-6814-48f1-a921-f9bd953fc9d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "638e50ad-cce1-4c91-8b3f-8e280d91d669",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "15caef2e-8fbd-4796-8a3d-83eb591db207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "0b2625fd-6fd9-49d8-85fa-261009d1a253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15caef2e-8fbd-4796-8a3d-83eb591db207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b519a38f-aca5-4e20-aee0-1126c9724143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15caef2e-8fbd-4796-8a3d-83eb591db207",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "3798fd79-7acc-4bb2-9059-17baedb5d52e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "99953598-da28-42b4-96a2-fdc96b731250",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3798fd79-7acc-4bb2-9059-17baedb5d52e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69371917-1082-4dc6-bf22-4643801e985d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3798fd79-7acc-4bb2-9059-17baedb5d52e",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "ca7e16db-7468-4c1f-b79c-2c62a1762e0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "7f95b968-ff56-40b7-830e-309e897a49e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca7e16db-7468-4c1f-b79c-2c62a1762e0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dd3520b-fad3-45bd-9eb3-374f449a9b29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca7e16db-7468-4c1f-b79c-2c62a1762e0b",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "2ec08b4d-9a90-4526-bf0f-37799b0cb2db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "1f734509-24b7-4768-8895-b903ebc241f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ec08b4d-9a90-4526-bf0f-37799b0cb2db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f0dc7b1-29ac-46c6-ac2a-fdee256d52cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ec08b4d-9a90-4526-bf0f-37799b0cb2db",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "a1120e9f-5c96-4974-9397-b751ba61c368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "13d43a35-8d8a-44df-bde0-34198a0d8ec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1120e9f-5c96-4974-9397-b751ba61c368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3352b23-e7b1-44c7-9d72-48a3eae641c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1120e9f-5c96-4974-9397-b751ba61c368",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "25a65b38-dc79-49e9-a978-127d430f1dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "d152deee-ce2c-416b-91f3-d17bfe8712f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a65b38-dc79-49e9-a978-127d430f1dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17979f10-388e-4318-8c77-0c7a87a39e6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a65b38-dc79-49e9-a978-127d430f1dfc",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "00cc3949-6df8-479f-a4f7-7ed47297d753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "c97ab747-50d7-4daa-b988-00046717f6d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00cc3949-6df8-479f-a4f7-7ed47297d753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3baba8-b276-40ef-9008-0ebfe8662882",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00cc3949-6df8-479f-a4f7-7ed47297d753",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "d9c41897-6a6d-4619-b063-240a3812025a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "4cfa9fc8-124f-4ccb-8ffc-949e5dcfcac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9c41897-6a6d-4619-b063-240a3812025a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f98b16e0-f5a9-493e-9176-fe5dc8651411",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9c41897-6a6d-4619-b063-240a3812025a",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "cd565d41-9f85-4f16-ae09-c71a51613d28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "58f7752d-1727-477d-b03b-35e20b984c45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd565d41-9f85-4f16-ae09-c71a51613d28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44d0f8d1-e55a-4050-9bdb-8c2130fb94ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd565d41-9f85-4f16-ae09-c71a51613d28",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        },
        {
            "id": "5fa0696d-6aef-4df7-9efd-02d620e63649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "compositeImage": {
                "id": "8bde9847-203a-43eb-b3c6-eab0efc8b981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa0696d-6aef-4df7-9efd-02d620e63649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44c7140e-a179-4eb2-89ad-bea143d8f3d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa0696d-6aef-4df7-9efd-02d620e63649",
                    "LayerId": "96855471-5be1-4947-a50a-f8f45620025e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "96855471-5be1-4947-a50a-f8f45620025e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acaf32c6-d051-4d5c-8b1b-19accdd71ccb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}