{
    "id": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wolfImpactEffect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "485faf6c-17f5-4f6d-8e0e-264c959a9d84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "d164e29e-8ed7-45c6-9000-0055d9684e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "485faf6c-17f5-4f6d-8e0e-264c959a9d84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae009aa9-c81b-4131-9693-3a27fd88437f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "485faf6c-17f5-4f6d-8e0e-264c959a9d84",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        },
        {
            "id": "e30659e5-7816-4f0e-9e82-16f18e253bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "44d396ad-c19c-4449-9f0a-4337395edae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e30659e5-7816-4f0e-9e82-16f18e253bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5954e33-2b36-4678-a05e-98f0a0c91c07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e30659e5-7816-4f0e-9e82-16f18e253bed",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        },
        {
            "id": "8662af5e-acb2-419c-957f-15117749200e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "0f5caddb-517b-45b0-b9da-74593293fde9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8662af5e-acb2-419c-957f-15117749200e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "120e7365-d201-4993-930b-8525f241a391",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8662af5e-acb2-419c-957f-15117749200e",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        },
        {
            "id": "3feec470-71d7-4ad1-9e31-79d994107495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "64cde33d-66fd-4dc8-9e20-578036cbf5e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3feec470-71d7-4ad1-9e31-79d994107495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d750c5a2-135d-4c1a-a1f4-2c05e9bc7f70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3feec470-71d7-4ad1-9e31-79d994107495",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        },
        {
            "id": "b87cba4b-e06e-4d88-aac9-b4eebd2e6079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "5303526c-7951-4092-af57-eaf170fb18c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b87cba4b-e06e-4d88-aac9-b4eebd2e6079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf04f87-6dd4-4a8e-aac8-7c87ca4d8caf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b87cba4b-e06e-4d88-aac9-b4eebd2e6079",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        },
        {
            "id": "16418ed4-bb6b-4792-900f-d831050267db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "06578ef4-9840-45c3-bdf4-550a0921ae42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16418ed4-bb6b-4792-900f-d831050267db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3789f97d-1ef4-49c3-b760-3ec6872d85fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16418ed4-bb6b-4792-900f-d831050267db",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        },
        {
            "id": "c94249a6-05b1-4c8e-b890-d9b5d794cb10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "6fa6c057-5247-4fce-bb96-4904b4ed5dd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c94249a6-05b1-4c8e-b890-d9b5d794cb10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f71c639-0eeb-4727-a283-9fd9bc32d26b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c94249a6-05b1-4c8e-b890-d9b5d794cb10",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        },
        {
            "id": "de4167e7-5f43-4a9b-b581-d9ace8a0d7c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "62df12a9-befd-405d-a2cd-6a658401305b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de4167e7-5f43-4a9b-b581-d9ace8a0d7c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b16c9daf-653f-4f2c-8f65-84576e30b247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de4167e7-5f43-4a9b-b581-d9ace8a0d7c6",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        },
        {
            "id": "3f8b483e-01a2-4feb-bb30-6f5cb54761b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "5472ba77-51e4-45cb-a171-c1664901e1df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f8b483e-01a2-4feb-bb30-6f5cb54761b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff92d92-6745-467e-bade-911f8f3d2171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f8b483e-01a2-4feb-bb30-6f5cb54761b8",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        },
        {
            "id": "fe985be5-c153-4233-a382-26296cf66b95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "38a4e8e0-9df4-4e8a-b47b-72cff73c19d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe985be5-c153-4233-a382-26296cf66b95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93048f58-c5b1-455c-91a6-ece079423fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe985be5-c153-4233-a382-26296cf66b95",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        },
        {
            "id": "fe0a6f94-1a3a-4c13-882d-b03fad1c2510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "compositeImage": {
                "id": "658b0135-9a5c-4241-a453-156c5aa28db5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe0a6f94-1a3a-4c13-882d-b03fad1c2510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "798bbaee-18ea-4afd-a283-ba6191d44272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe0a6f94-1a3a-4c13-882d-b03fad1c2510",
                    "LayerId": "39ab0737-441d-41a8-be71-ab6edbdbbf88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "39ab0737-441d-41a8-be71-ab6edbdbbf88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c63d86bb-c488-4a9b-a9b3-44096fa1263e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}