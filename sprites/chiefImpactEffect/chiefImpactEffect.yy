{
    "id": "2d513349-5b43-499f-b60a-4641d864c979",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "chiefImpactEffect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 275,
    "bbox_left": 20,
    "bbox_right": 267,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f40c2eea-701c-4fe5-b543-4305c866987b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "6f60c6e0-5f13-4134-a198-9f81b83ca8fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f40c2eea-701c-4fe5-b543-4305c866987b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa579d30-cd33-4f63-a543-04856b01d071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f40c2eea-701c-4fe5-b543-4305c866987b",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        },
        {
            "id": "95ddec29-e180-4260-b3e0-786fcb09e89a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "68659acb-34cf-449c-b878-05ae51db7f28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95ddec29-e180-4260-b3e0-786fcb09e89a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd731b08-1499-48fe-a6a4-88f32d445be6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95ddec29-e180-4260-b3e0-786fcb09e89a",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        },
        {
            "id": "13538964-9dd2-433f-a59f-4bcfbec503ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "54781bba-453a-43b3-9b83-11516d2707e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13538964-9dd2-433f-a59f-4bcfbec503ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0157271a-e008-473e-8674-96751f347e3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13538964-9dd2-433f-a59f-4bcfbec503ae",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        },
        {
            "id": "a32d67fc-6f9c-446c-bd4f-b002f14702af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "6de75e63-2d2c-40a9-b94d-532c6be4f970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a32d67fc-6f9c-446c-bd4f-b002f14702af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd47430d-fadc-4f31-9a14-3f1f4a09932a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a32d67fc-6f9c-446c-bd4f-b002f14702af",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        },
        {
            "id": "8d71f820-5e2a-4e8c-8b0a-937efe64e330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "6c025e76-5b33-4919-a69b-d41126b7afa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d71f820-5e2a-4e8c-8b0a-937efe64e330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ed2e4e5-1a2a-4210-a0df-64b0971a9a67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d71f820-5e2a-4e8c-8b0a-937efe64e330",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        },
        {
            "id": "5673b9a8-1a9f-41ce-a905-99a45ed81eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "49071f94-caa4-43c2-8753-5104e064e692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5673b9a8-1a9f-41ce-a905-99a45ed81eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bddf968-cfc4-49e0-86e9-0790537be80e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5673b9a8-1a9f-41ce-a905-99a45ed81eb3",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        },
        {
            "id": "46d1d67a-d47c-44e8-a6ff-bbcb55e2c77a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "db820fa3-c032-47d9-aad1-07fa3d1fadc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d1d67a-d47c-44e8-a6ff-bbcb55e2c77a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18273f37-37de-46a7-854a-ed3d5def54ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d1d67a-d47c-44e8-a6ff-bbcb55e2c77a",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        },
        {
            "id": "2564fb17-6151-4c13-9a5e-e916b500d559",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "7ae6701f-bd2c-4f19-aeeb-3d3c609daf87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2564fb17-6151-4c13-9a5e-e916b500d559",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aebd5ab2-7fcd-42d3-a305-f8959a8dc871",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2564fb17-6151-4c13-9a5e-e916b500d559",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        },
        {
            "id": "0ecf4ccf-936e-4c56-9c2e-3dc36458e739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "6e99cc3a-63b6-4bf6-8799-2565f70465ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ecf4ccf-936e-4c56-9c2e-3dc36458e739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7b1840d-f2b2-435a-8c77-4153efce80c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ecf4ccf-936e-4c56-9c2e-3dc36458e739",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        },
        {
            "id": "badc6390-d09a-4262-913d-0817db14b65f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "35c23a53-6b62-4deb-97b2-55791eecf444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "badc6390-d09a-4262-913d-0817db14b65f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a9bd11-efe5-4f7e-8784-38bb2c0f2f30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "badc6390-d09a-4262-913d-0817db14b65f",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        },
        {
            "id": "c98f6f58-70f7-4862-b210-5599ab3c9f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "compositeImage": {
                "id": "29d2e49d-0527-4ec7-9838-b13685acb16e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c98f6f58-70f7-4862-b210-5599ab3c9f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb83ee45-b97f-422c-9213-09bcbdb63921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c98f6f58-70f7-4862-b210-5599ab3c9f6e",
                    "LayerId": "647dc972-36fc-4b40-8e37-d295e7e8b62b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "647dc972-36fc-4b40-8e37-d295e7e8b62b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d513349-5b43-499f-b60a-4641d864c979",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 144,
    "yorig": 144
}