{
    "id": "ea6fe926-ba3e-40c1-a408-52592fcfdb69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharacterFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 287,
    "bbox_left": 0,
    "bbox_right": 690,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6973b8a4-67bf-4f15-a862-31a2665ca81c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea6fe926-ba3e-40c1-a408-52592fcfdb69",
            "compositeImage": {
                "id": "5f6e7733-5d59-4548-a4d8-063429fec5d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6973b8a4-67bf-4f15-a862-31a2665ca81c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a6937ab-abee-4bec-8edd-40f935321b28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6973b8a4-67bf-4f15-a862-31a2665ca81c",
                    "LayerId": "d5313bb0-d191-45be-bec5-e1f6ca6744df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 290,
    "layers": [
        {
            "id": "d5313bb0-d191-45be-bec5-e1f6ca6744df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea6fe926-ba3e-40c1-a408-52592fcfdb69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 692,
    "xorig": 0,
    "yorig": 0
}