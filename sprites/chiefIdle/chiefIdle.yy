{
    "id": "88b245e5-b940-482e-bc2a-55e1a52a82d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "chiefIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 16,
    "bbox_right": 175,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6dd32d81-c9b4-4838-81e3-cb7c451e8bce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b245e5-b940-482e-bc2a-55e1a52a82d9",
            "compositeImage": {
                "id": "8044da9e-3aef-4240-8fe7-bbd53735196f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd32d81-c9b4-4838-81e3-cb7c451e8bce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69e9d4d1-cd75-466e-b0f3-33a7652ea750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd32d81-c9b4-4838-81e3-cb7c451e8bce",
                    "LayerId": "d11d4ec4-c6b3-4359-b5a0-484e207a1ae7"
                }
            ]
        },
        {
            "id": "a48f5b58-3c31-4cda-95e0-26e054500cbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b245e5-b940-482e-bc2a-55e1a52a82d9",
            "compositeImage": {
                "id": "e7533fc9-0848-4812-aef2-96688f26c758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a48f5b58-3c31-4cda-95e0-26e054500cbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "228e35bd-8c3d-41b0-8f80-02f9992fe967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48f5b58-3c31-4cda-95e0-26e054500cbb",
                    "LayerId": "d11d4ec4-c6b3-4359-b5a0-484e207a1ae7"
                }
            ]
        },
        {
            "id": "bc699a84-408f-4f5c-8a13-8be44a386c69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b245e5-b940-482e-bc2a-55e1a52a82d9",
            "compositeImage": {
                "id": "c3766b4f-9f4c-48dc-b27e-1da7a68e0e99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc699a84-408f-4f5c-8a13-8be44a386c69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ac24c77-2a2e-4344-9304-b64d278ad2bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc699a84-408f-4f5c-8a13-8be44a386c69",
                    "LayerId": "d11d4ec4-c6b3-4359-b5a0-484e207a1ae7"
                }
            ]
        },
        {
            "id": "bbc02935-9f43-413d-837e-00fcd86bdbf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b245e5-b940-482e-bc2a-55e1a52a82d9",
            "compositeImage": {
                "id": "a12aece0-487c-49fc-ad7c-5fcb445e60a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc02935-9f43-413d-837e-00fcd86bdbf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39e1e7fc-ce59-4cc0-a23b-a4d27a39d7b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc02935-9f43-413d-837e-00fcd86bdbf9",
                    "LayerId": "d11d4ec4-c6b3-4359-b5a0-484e207a1ae7"
                }
            ]
        },
        {
            "id": "27849f26-025a-4ebc-8aa4-6fcc5d51c991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b245e5-b940-482e-bc2a-55e1a52a82d9",
            "compositeImage": {
                "id": "f567c13c-f69d-4164-a5f2-59a18e5d52ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27849f26-025a-4ebc-8aa4-6fcc5d51c991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6182b12-69d0-477a-8e96-1fe8b5671581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27849f26-025a-4ebc-8aa4-6fcc5d51c991",
                    "LayerId": "d11d4ec4-c6b3-4359-b5a0-484e207a1ae7"
                }
            ]
        },
        {
            "id": "d979d77f-2a02-40a4-95b2-0e8bfd312800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b245e5-b940-482e-bc2a-55e1a52a82d9",
            "compositeImage": {
                "id": "736a08a4-a18e-4239-8a70-cfe9128fd335",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d979d77f-2a02-40a4-95b2-0e8bfd312800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51ce347c-0fd4-4f3d-b0d2-b6342b6aceea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d979d77f-2a02-40a4-95b2-0e8bfd312800",
                    "LayerId": "d11d4ec4-c6b3-4359-b5a0-484e207a1ae7"
                }
            ]
        },
        {
            "id": "7eff3277-a8b1-4445-9eba-d8826d849d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b245e5-b940-482e-bc2a-55e1a52a82d9",
            "compositeImage": {
                "id": "f2d94b3c-6e3f-44ce-a084-9038f16fd46c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eff3277-a8b1-4445-9eba-d8826d849d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e95f1a82-c67a-43a0-b7e0-397401e80b29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eff3277-a8b1-4445-9eba-d8826d849d8d",
                    "LayerId": "d11d4ec4-c6b3-4359-b5a0-484e207a1ae7"
                }
            ]
        },
        {
            "id": "febfd8cd-e77f-4fde-8db5-4bfa66fae0e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b245e5-b940-482e-bc2a-55e1a52a82d9",
            "compositeImage": {
                "id": "d92c5ff7-631b-437c-a605-38cdd2fc9686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "febfd8cd-e77f-4fde-8db5-4bfa66fae0e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d1349cc-6608-4212-9ec7-71e765a80f51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "febfd8cd-e77f-4fde-8db5-4bfa66fae0e7",
                    "LayerId": "d11d4ec4-c6b3-4359-b5a0-484e207a1ae7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "d11d4ec4-c6b3-4359-b5a0-484e207a1ae7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88b245e5-b940-482e-bc2a-55e1a52a82d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}