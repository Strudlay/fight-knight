{
    "id": "caaa3f90-13a5-4205-9647-b74667c2c08c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_pillars_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 391,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f99296e-f370-4e78-8388-18c8a7f96ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caaa3f90-13a5-4205-9647-b74667c2c08c",
            "compositeImage": {
                "id": "f9bc885c-201b-41c0-bb16-63c76a58880d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f99296e-f370-4e78-8388-18c8a7f96ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61d93452-57d8-4385-b116-8e887c3f98d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f99296e-f370-4e78-8388-18c8a7f96ab5",
                    "LayerId": "2d80bb2a-0e9c-4aca-9a64-985c324bbb8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "2d80bb2a-0e9c-4aca-9a64-985c324bbb8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "caaa3f90-13a5-4205-9647-b74667c2c08c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 392,
    "xorig": 0,
    "yorig": 0
}