{
    "id": "d6df9b78-051c-47f5-a178-beaea450c889",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "knightImpactEffect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 215,
    "bbox_left": 28,
    "bbox_right": 215,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76016923-bc48-4763-9d7c-d64826300c38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6df9b78-051c-47f5-a178-beaea450c889",
            "compositeImage": {
                "id": "adfc9f15-96e2-48c2-9079-b6946d06aeb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76016923-bc48-4763-9d7c-d64826300c38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f976d2d9-384f-4aa5-8ec7-852e4e6645ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76016923-bc48-4763-9d7c-d64826300c38",
                    "LayerId": "6ee57385-4c90-4aae-b79d-e9fb6c1ecc64"
                }
            ]
        },
        {
            "id": "2bf19cbb-7bc3-4f0a-88fe-a91df117adb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6df9b78-051c-47f5-a178-beaea450c889",
            "compositeImage": {
                "id": "1097fed8-994b-474c-9d0f-e24b142b3794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bf19cbb-7bc3-4f0a-88fe-a91df117adb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb39e970-a875-4f61-a7d4-f3f9e536327f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bf19cbb-7bc3-4f0a-88fe-a91df117adb0",
                    "LayerId": "6ee57385-4c90-4aae-b79d-e9fb6c1ecc64"
                }
            ]
        },
        {
            "id": "04817759-35c7-4dea-88a4-2a1d1c76cafc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6df9b78-051c-47f5-a178-beaea450c889",
            "compositeImage": {
                "id": "adf2de23-084d-4d68-bfcd-1cc3203c9f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04817759-35c7-4dea-88a4-2a1d1c76cafc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "466401c4-b1a3-453b-8913-1e60e4a4238e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04817759-35c7-4dea-88a4-2a1d1c76cafc",
                    "LayerId": "6ee57385-4c90-4aae-b79d-e9fb6c1ecc64"
                }
            ]
        },
        {
            "id": "aa615064-1c29-49a9-bc32-81dd980cdb74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6df9b78-051c-47f5-a178-beaea450c889",
            "compositeImage": {
                "id": "3f85c894-81e6-4018-84d3-72305480e0f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa615064-1c29-49a9-bc32-81dd980cdb74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51bc52db-bf93-4cdc-a8ff-bd1fed0f91d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa615064-1c29-49a9-bc32-81dd980cdb74",
                    "LayerId": "6ee57385-4c90-4aae-b79d-e9fb6c1ecc64"
                }
            ]
        },
        {
            "id": "a8e37b5c-935b-45e2-b36c-d937ead39da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6df9b78-051c-47f5-a178-beaea450c889",
            "compositeImage": {
                "id": "6b8c97d1-3428-41aa-843a-eb2d92aaeb69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8e37b5c-935b-45e2-b36c-d937ead39da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "455e9124-db79-422a-911d-c7341a023e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8e37b5c-935b-45e2-b36c-d937ead39da2",
                    "LayerId": "6ee57385-4c90-4aae-b79d-e9fb6c1ecc64"
                }
            ]
        },
        {
            "id": "84984550-945c-4e47-8dc5-18b5a6c64b2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6df9b78-051c-47f5-a178-beaea450c889",
            "compositeImage": {
                "id": "e6dc4a7b-280c-4d13-bca0-b38610109566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84984550-945c-4e47-8dc5-18b5a6c64b2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02d69da1-ad65-4e42-bf15-ba7a3a7053a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84984550-945c-4e47-8dc5-18b5a6c64b2b",
                    "LayerId": "6ee57385-4c90-4aae-b79d-e9fb6c1ecc64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "6ee57385-4c90-4aae-b79d-e9fb6c1ecc64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6df9b78-051c-47f5-a178-beaea450c889",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 120
}