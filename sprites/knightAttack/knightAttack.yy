{
    "id": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "knightAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 8,
    "bbox_right": 383,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fafad7e2-82d6-48dc-869b-96cd96c97321",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "88ac1e45-6c65-4689-9195-f843750aecb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fafad7e2-82d6-48dc-869b-96cd96c97321",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c6107cf-cf5a-4c8f-ad5b-4eba62e54f86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fafad7e2-82d6-48dc-869b-96cd96c97321",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "a5905b24-05b2-45c9-93f4-913a2f87963e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "54e5435a-38cc-4fa8-99d9-100c452c27a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5905b24-05b2-45c9-93f4-913a2f87963e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19194d35-1b94-4b57-a4ee-c039d882602c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5905b24-05b2-45c9-93f4-913a2f87963e",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "be9e3dcf-299f-43c4-946e-fa10142645d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "66736fb1-1424-4826-9fff-155814aa593e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be9e3dcf-299f-43c4-946e-fa10142645d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ef15d4b-54c8-4db5-b561-94329ebd6154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be9e3dcf-299f-43c4-946e-fa10142645d9",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "c9ba4222-fd07-4366-8c49-1e210488172d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "38a3d8f1-47db-4a61-b6bc-43790fdd342b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9ba4222-fd07-4366-8c49-1e210488172d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adfa26e6-0fb7-4197-a488-acd1237f3068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9ba4222-fd07-4366-8c49-1e210488172d",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "6c6a7149-6ab6-4cc8-892d-b8aaa5ac143b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "71f42992-7e37-49c8-a78a-f16a36e0f2ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c6a7149-6ab6-4cc8-892d-b8aaa5ac143b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78c5edcf-fc0f-4e5e-ace9-c99094325c61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c6a7149-6ab6-4cc8-892d-b8aaa5ac143b",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "b3de1346-5396-4128-a05f-9d848d8b1b16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "c54c7ee3-2bff-475d-80aa-e77a22166b58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3de1346-5396-4128-a05f-9d848d8b1b16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49e30cc-9929-4858-81f8-3d9c41cadb85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3de1346-5396-4128-a05f-9d848d8b1b16",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "b1d4a8ee-942a-425b-b981-b7b920170583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "960aadfd-6d95-4200-8c6e-ebd0e5c270b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1d4a8ee-942a-425b-b981-b7b920170583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba4073ce-0bcd-4c40-87fc-ce9a7d9e33ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1d4a8ee-942a-425b-b981-b7b920170583",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "acb93773-6f12-4973-a281-e9c8fe2d6f6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "c77d462b-0934-464d-9a6f-9196a2ec9f3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acb93773-6f12-4973-a281-e9c8fe2d6f6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9740adf0-caae-4566-8b93-069a06b0b1fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acb93773-6f12-4973-a281-e9c8fe2d6f6b",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "dd035218-9785-4791-83af-fcb203914e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "ffa076c2-8ed0-4f7e-afc3-e7f32a1162e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd035218-9785-4791-83af-fcb203914e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb0c327f-1df4-4e39-ab9b-f73728a05e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd035218-9785-4791-83af-fcb203914e8e",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "dd13f62c-9bff-4f42-b1f4-a928ed32383b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "a3b755ee-358b-4ed4-aaa8-c18d274e5d45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd13f62c-9bff-4f42-b1f4-a928ed32383b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48b43890-73ab-48d5-8f35-fb6b675d4489",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd13f62c-9bff-4f42-b1f4-a928ed32383b",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "4bc315c9-4afe-4d2d-838b-9f3a115134e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "fd823b70-e751-4c57-a506-06baadfa667d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bc315c9-4afe-4d2d-838b-9f3a115134e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "467c4270-961d-4e56-b63e-c2171cd76ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bc315c9-4afe-4d2d-838b-9f3a115134e4",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "96d02956-3b66-41f5-8d82-1252d9120e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "79a425b3-6f35-4c3e-91c1-1d08578117d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d02956-3b66-41f5-8d82-1252d9120e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e8d493-4dc2-4aef-be13-5b20fd400063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d02956-3b66-41f5-8d82-1252d9120e50",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "ebd221cf-d7be-4574-9cef-fb787df13cd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "2d54b9da-1c54-41e4-8223-b6636a36f0e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebd221cf-d7be-4574-9cef-fb787df13cd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c13eeec8-33c0-4f68-82b0-acea2a1fbfb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebd221cf-d7be-4574-9cef-fb787df13cd6",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        },
        {
            "id": "48b5909e-5759-49a2-8658-8e43e95e2caa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "compositeImage": {
                "id": "cc0a4ae2-46dd-4a26-b481-1c7209fadbc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48b5909e-5759-49a2-8658-8e43e95e2caa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1adddc4b-2920-4200-9a39-d64ee7a78cc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b5909e-5759-49a2-8658-8e43e95e2caa",
                    "LayerId": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "4d61ab03-f3e5-4b66-8c4c-023c9c9e19ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "485b6468-c1f2-42e2-8445-1c8039a1dd32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 165,
    "yorig": 95
}