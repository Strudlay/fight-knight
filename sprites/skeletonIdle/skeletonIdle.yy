{
    "id": "cfa0278e-6324-4417-ac7c-b38ab0196f1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeletonIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 20,
    "bbox_right": 167,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6de711d-fc51-4d18-99e6-9b63a8ee8cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfa0278e-6324-4417-ac7c-b38ab0196f1d",
            "compositeImage": {
                "id": "b28928bc-3cf7-4ed6-9c0c-56dd384a0678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6de711d-fc51-4d18-99e6-9b63a8ee8cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e976e026-3df9-43b9-a9d4-4437722c78c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6de711d-fc51-4d18-99e6-9b63a8ee8cea",
                    "LayerId": "471daa4b-2af7-4a85-84ae-a690e084c0dd"
                }
            ]
        },
        {
            "id": "784ab207-6673-4841-9fd0-dae74e38e1a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfa0278e-6324-4417-ac7c-b38ab0196f1d",
            "compositeImage": {
                "id": "2cd82856-8e69-4ef0-ad55-b700cf9252ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "784ab207-6673-4841-9fd0-dae74e38e1a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "958e5bbb-08b9-4a3d-85a6-74992d6040f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "784ab207-6673-4841-9fd0-dae74e38e1a4",
                    "LayerId": "471daa4b-2af7-4a85-84ae-a690e084c0dd"
                }
            ]
        },
        {
            "id": "0b0355f7-b7a5-474d-b952-4f186887a054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfa0278e-6324-4417-ac7c-b38ab0196f1d",
            "compositeImage": {
                "id": "78bf430e-bd4f-4a4d-ad8d-a9b250c015b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b0355f7-b7a5-474d-b952-4f186887a054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b0e7fe4-dd2c-4f6b-98c1-07af08d1a878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b0355f7-b7a5-474d-b952-4f186887a054",
                    "LayerId": "471daa4b-2af7-4a85-84ae-a690e084c0dd"
                }
            ]
        },
        {
            "id": "0d3d9588-e253-4a7f-8b50-d9e49c27acd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfa0278e-6324-4417-ac7c-b38ab0196f1d",
            "compositeImage": {
                "id": "f4a62055-ec5c-43ee-93e9-8a06633e4115",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d3d9588-e253-4a7f-8b50-d9e49c27acd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f813531-3834-4bdb-b138-05d756c2dad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d3d9588-e253-4a7f-8b50-d9e49c27acd0",
                    "LayerId": "471daa4b-2af7-4a85-84ae-a690e084c0dd"
                }
            ]
        },
        {
            "id": "d017197c-22c5-4c63-8269-998fcf186d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfa0278e-6324-4417-ac7c-b38ab0196f1d",
            "compositeImage": {
                "id": "e32ea896-5058-4d8e-8ad6-3eb743115c58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d017197c-22c5-4c63-8269-998fcf186d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ea5075-f60a-40a6-9541-8d940d2b436f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d017197c-22c5-4c63-8269-998fcf186d78",
                    "LayerId": "471daa4b-2af7-4a85-84ae-a690e084c0dd"
                }
            ]
        },
        {
            "id": "7901f9ce-e760-4159-8ed0-533cb2f020e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfa0278e-6324-4417-ac7c-b38ab0196f1d",
            "compositeImage": {
                "id": "d4c5ac1e-1f1a-4da8-bad5-8bd83212bcda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7901f9ce-e760-4159-8ed0-533cb2f020e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfe7e4fb-e02e-4d19-91a0-185d59ce1ff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7901f9ce-e760-4159-8ed0-533cb2f020e0",
                    "LayerId": "471daa4b-2af7-4a85-84ae-a690e084c0dd"
                }
            ]
        },
        {
            "id": "a1c69394-9082-40bd-8cbb-6d8e146f4979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfa0278e-6324-4417-ac7c-b38ab0196f1d",
            "compositeImage": {
                "id": "d0d83d8d-c22f-497f-b504-c197a54caaa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1c69394-9082-40bd-8cbb-6d8e146f4979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3731a90-1de5-48e3-a81c-31615557f684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1c69394-9082-40bd-8cbb-6d8e146f4979",
                    "LayerId": "471daa4b-2af7-4a85-84ae-a690e084c0dd"
                }
            ]
        },
        {
            "id": "d0377de0-5ab4-4ff5-a123-0d59ab742b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfa0278e-6324-4417-ac7c-b38ab0196f1d",
            "compositeImage": {
                "id": "b187f8b7-47c7-4079-b045-469f3e5d0084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0377de0-5ab4-4ff5-a123-0d59ab742b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b84807d6-da71-43b9-903a-af9556a0642d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0377de0-5ab4-4ff5-a123-0d59ab742b48",
                    "LayerId": "471daa4b-2af7-4a85-84ae-a690e084c0dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "471daa4b-2af7-4a85-84ae-a690e084c0dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfa0278e-6324-4417-ac7c-b38ab0196f1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}