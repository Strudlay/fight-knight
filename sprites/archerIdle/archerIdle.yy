{
    "id": "b0a426bc-d3c1-4315-bfce-a08cc53f66b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "archerIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 36,
    "bbox_right": 151,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c4c417d-5937-4867-9828-774fef7ef732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0a426bc-d3c1-4315-bfce-a08cc53f66b1",
            "compositeImage": {
                "id": "6c241b2a-66b6-4460-9f01-8346bceace7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c4c417d-5937-4867-9828-774fef7ef732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4904a791-5067-4e58-a203-a76871ab8a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c4c417d-5937-4867-9828-774fef7ef732",
                    "LayerId": "a0d9ff68-f6dd-4b01-a025-587bae3af185"
                }
            ]
        },
        {
            "id": "1cee245c-fb2f-45d1-a6fd-c8e6721d8331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0a426bc-d3c1-4315-bfce-a08cc53f66b1",
            "compositeImage": {
                "id": "31775f63-d786-469e-b316-d267b97fee84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cee245c-fb2f-45d1-a6fd-c8e6721d8331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2e84da4-9f8b-4e96-8b4a-67fa5195451e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cee245c-fb2f-45d1-a6fd-c8e6721d8331",
                    "LayerId": "a0d9ff68-f6dd-4b01-a025-587bae3af185"
                }
            ]
        },
        {
            "id": "943a3ff0-20ab-4fa0-bc0d-1b06538935d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0a426bc-d3c1-4315-bfce-a08cc53f66b1",
            "compositeImage": {
                "id": "94b7e9c6-6acf-405f-929a-3dd83b5ebb39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "943a3ff0-20ab-4fa0-bc0d-1b06538935d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e574ad10-c046-476a-8a9d-81377469cb61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "943a3ff0-20ab-4fa0-bc0d-1b06538935d0",
                    "LayerId": "a0d9ff68-f6dd-4b01-a025-587bae3af185"
                }
            ]
        },
        {
            "id": "58ec2767-81f7-4c64-9fb5-ded419fef805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0a426bc-d3c1-4315-bfce-a08cc53f66b1",
            "compositeImage": {
                "id": "cc047626-bd4b-49d5-a875-4f602abd4783",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58ec2767-81f7-4c64-9fb5-ded419fef805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f70a963-9d2c-48bf-820c-e36cbd0dc946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58ec2767-81f7-4c64-9fb5-ded419fef805",
                    "LayerId": "a0d9ff68-f6dd-4b01-a025-587bae3af185"
                }
            ]
        },
        {
            "id": "929f913b-d67b-4b8d-ac98-1e7bfebc9284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0a426bc-d3c1-4315-bfce-a08cc53f66b1",
            "compositeImage": {
                "id": "f6c16d44-1174-4873-9d84-2f6bf6d3db92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "929f913b-d67b-4b8d-ac98-1e7bfebc9284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "926d9a17-fe7a-4b2b-b565-5f920e9ef8fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "929f913b-d67b-4b8d-ac98-1e7bfebc9284",
                    "LayerId": "a0d9ff68-f6dd-4b01-a025-587bae3af185"
                }
            ]
        },
        {
            "id": "f2c5d243-f94b-43ea-9bcd-f875f4ddb395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0a426bc-d3c1-4315-bfce-a08cc53f66b1",
            "compositeImage": {
                "id": "ca42c577-e2e9-4fd0-ac75-27f119315423",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c5d243-f94b-43ea-9bcd-f875f4ddb395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f40483b-b61a-4bae-88af-d34d61b3a64a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c5d243-f94b-43ea-9bcd-f875f4ddb395",
                    "LayerId": "a0d9ff68-f6dd-4b01-a025-587bae3af185"
                }
            ]
        },
        {
            "id": "4e2651a5-09be-4ae3-9272-4f758c1989a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0a426bc-d3c1-4315-bfce-a08cc53f66b1",
            "compositeImage": {
                "id": "81e4a9b2-beb7-4ba6-ba05-31a1da290370",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e2651a5-09be-4ae3-9272-4f758c1989a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d521584-20e3-44c4-8293-818f4cfec6db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e2651a5-09be-4ae3-9272-4f758c1989a7",
                    "LayerId": "a0d9ff68-f6dd-4b01-a025-587bae3af185"
                }
            ]
        },
        {
            "id": "8aa3bd78-0c23-4ab4-a1a6-ddb35a74d7c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0a426bc-d3c1-4315-bfce-a08cc53f66b1",
            "compositeImage": {
                "id": "5e4d752a-1cbb-4865-9e22-dc7b5a25ce20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa3bd78-0c23-4ab4-a1a6-ddb35a74d7c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa965fd6-954a-4e9a-ac44-20f36d5e9449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa3bd78-0c23-4ab4-a1a6-ddb35a74d7c0",
                    "LayerId": "a0d9ff68-f6dd-4b01-a025-587bae3af185"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "a0d9ff68-f6dd-4b01-a025-587bae3af185",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0a426bc-d3c1-4315-bfce-a08cc53f66b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}