{
    "id": "1d188960-63b0-4d68-a1f7-93ec69bf9f03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSelectionFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3373,
    "bbox_left": 0,
    "bbox_right": 2475,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f8b7397-be02-429c-b38d-9319e5e993c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d188960-63b0-4d68-a1f7-93ec69bf9f03",
            "compositeImage": {
                "id": "79fdf6a2-2da7-4d71-8020-91d314877bc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f8b7397-be02-429c-b38d-9319e5e993c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd01360-1744-4136-9d30-2d624fc156e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8b7397-be02-429c-b38d-9319e5e993c5",
                    "LayerId": "e05e1b99-416e-46de-810c-d40826b1cd13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3374,
    "layers": [
        {
            "id": "e05e1b99-416e-46de-810c-d40826b1cd13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d188960-63b0-4d68-a1f7-93ec69bf9f03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2476,
    "xorig": 1238,
    "yorig": 1687
}