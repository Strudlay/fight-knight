{
    "id": "c8056ebf-5afd-4b14-9068-c5265fef0f16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mageWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 28,
    "bbox_right": 119,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab39d338-fdae-4503-a238-6739a3615e92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8056ebf-5afd-4b14-9068-c5265fef0f16",
            "compositeImage": {
                "id": "a3081983-cf23-4a01-baa9-b0ef6c1d68d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab39d338-fdae-4503-a238-6739a3615e92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9422da5-2e81-4717-a471-c4bb2f021ff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab39d338-fdae-4503-a238-6739a3615e92",
                    "LayerId": "103ba50b-7432-4e21-a6c7-8a1da6cc22ab"
                }
            ]
        },
        {
            "id": "1ab90471-b043-4892-b643-1b8e0104201c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8056ebf-5afd-4b14-9068-c5265fef0f16",
            "compositeImage": {
                "id": "d6c1dc85-eab0-429f-8efb-3b58f2b71455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ab90471-b043-4892-b643-1b8e0104201c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c154ab38-2034-4349-9f25-72ad86b794b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ab90471-b043-4892-b643-1b8e0104201c",
                    "LayerId": "103ba50b-7432-4e21-a6c7-8a1da6cc22ab"
                }
            ]
        },
        {
            "id": "d6af19c1-f79e-4e65-9ea9-fea226b1d8bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8056ebf-5afd-4b14-9068-c5265fef0f16",
            "compositeImage": {
                "id": "54b829bc-1419-4382-a3e9-812d2f8ce21d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6af19c1-f79e-4e65-9ea9-fea226b1d8bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab34f66e-4c27-4af2-b090-eba2b5004781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6af19c1-f79e-4e65-9ea9-fea226b1d8bc",
                    "LayerId": "103ba50b-7432-4e21-a6c7-8a1da6cc22ab"
                }
            ]
        },
        {
            "id": "fe19dad9-770d-4d38-b76b-87c4befd233f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8056ebf-5afd-4b14-9068-c5265fef0f16",
            "compositeImage": {
                "id": "27f2ce54-d213-4100-b3c9-df3e59f3f4d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe19dad9-770d-4d38-b76b-87c4befd233f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "478d78d4-c1d4-4c15-9cf0-382dd5fb7fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe19dad9-770d-4d38-b76b-87c4befd233f",
                    "LayerId": "103ba50b-7432-4e21-a6c7-8a1da6cc22ab"
                }
            ]
        },
        {
            "id": "8c20222b-bb83-413e-80ac-c877968f72b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8056ebf-5afd-4b14-9068-c5265fef0f16",
            "compositeImage": {
                "id": "8eee7a35-3c60-4b36-8764-a00807c6be27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c20222b-bb83-413e-80ac-c877968f72b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2970831c-3365-4b86-b1da-5c8596653532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c20222b-bb83-413e-80ac-c877968f72b9",
                    "LayerId": "103ba50b-7432-4e21-a6c7-8a1da6cc22ab"
                }
            ]
        },
        {
            "id": "f91e41bb-34c8-4234-8bae-e5576818bb1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8056ebf-5afd-4b14-9068-c5265fef0f16",
            "compositeImage": {
                "id": "e7b36ddd-02be-46d1-8c8b-e85ead9aeae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f91e41bb-34c8-4234-8bae-e5576818bb1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f876d4-b952-4aea-9fbb-327a0db2b7fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f91e41bb-34c8-4234-8bae-e5576818bb1c",
                    "LayerId": "103ba50b-7432-4e21-a6c7-8a1da6cc22ab"
                }
            ]
        },
        {
            "id": "e51d1917-b60b-4737-88c2-0191f4326982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8056ebf-5afd-4b14-9068-c5265fef0f16",
            "compositeImage": {
                "id": "ef8cb73d-0a1e-4f02-aa2c-dec0d24d0536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e51d1917-b60b-4737-88c2-0191f4326982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec36d15a-c8b6-4135-8292-3aee34a9c0f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e51d1917-b60b-4737-88c2-0191f4326982",
                    "LayerId": "103ba50b-7432-4e21-a6c7-8a1da6cc22ab"
                }
            ]
        },
        {
            "id": "1db8e784-77da-4f3f-b57f-ccdd1a9264ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8056ebf-5afd-4b14-9068-c5265fef0f16",
            "compositeImage": {
                "id": "9b140834-c9e3-4b2b-8c57-6cae4ea3fe6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1db8e784-77da-4f3f-b57f-ccdd1a9264ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2edf5780-3f04-4d3b-bc29-0a2b54dc88eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1db8e784-77da-4f3f-b57f-ccdd1a9264ac",
                    "LayerId": "103ba50b-7432-4e21-a6c7-8a1da6cc22ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "103ba50b-7432-4e21-a6c7-8a1da6cc22ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8056ebf-5afd-4b14-9068-c5265fef0f16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}