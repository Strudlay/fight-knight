{
    "id": "e537476a-75ce-4697-b4dd-000856455dc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robotIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 100,
    "bbox_right": 287,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "081ccbe4-89e7-41bf-90d9-51f641eca669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e537476a-75ce-4697-b4dd-000856455dc9",
            "compositeImage": {
                "id": "6a0eca4b-de50-4ee9-ba09-cc3dcdaeda55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "081ccbe4-89e7-41bf-90d9-51f641eca669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bdd4202-e8c0-4e91-9c69-03af8e751020",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "081ccbe4-89e7-41bf-90d9-51f641eca669",
                    "LayerId": "6d421d9a-081a-4946-ad21-6eef126cda7a"
                }
            ]
        },
        {
            "id": "e9511482-2881-44bc-a9e9-5fb52c2c2175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e537476a-75ce-4697-b4dd-000856455dc9",
            "compositeImage": {
                "id": "df983351-db22-487a-aa8c-451c6dd4457a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9511482-2881-44bc-a9e9-5fb52c2c2175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab21dfde-5c6f-4770-a4eb-f836a33f1414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9511482-2881-44bc-a9e9-5fb52c2c2175",
                    "LayerId": "6d421d9a-081a-4946-ad21-6eef126cda7a"
                }
            ]
        },
        {
            "id": "f13918a7-e3b3-46df-ae4c-19846148c6ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e537476a-75ce-4697-b4dd-000856455dc9",
            "compositeImage": {
                "id": "3e3031ae-5855-4bab-aa16-509a967cfd37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f13918a7-e3b3-46df-ae4c-19846148c6ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85265688-e24e-4a65-80e4-e5ae0d192aab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13918a7-e3b3-46df-ae4c-19846148c6ae",
                    "LayerId": "6d421d9a-081a-4946-ad21-6eef126cda7a"
                }
            ]
        },
        {
            "id": "95383ed2-e1ef-4857-aea8-f0054e033e4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e537476a-75ce-4697-b4dd-000856455dc9",
            "compositeImage": {
                "id": "126b7b30-d63e-4a0b-8dbb-1fa357324871",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95383ed2-e1ef-4857-aea8-f0054e033e4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "512f8c17-ec41-4133-9ca8-b47e4780c920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95383ed2-e1ef-4857-aea8-f0054e033e4a",
                    "LayerId": "6d421d9a-081a-4946-ad21-6eef126cda7a"
                }
            ]
        },
        {
            "id": "3c62125c-b592-4f94-8f19-4ebaf1910731",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e537476a-75ce-4697-b4dd-000856455dc9",
            "compositeImage": {
                "id": "939f454f-14ed-4ae8-ac83-97daa75b8e6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c62125c-b592-4f94-8f19-4ebaf1910731",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1baf42d0-cea7-4745-a86e-65de4351055b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c62125c-b592-4f94-8f19-4ebaf1910731",
                    "LayerId": "6d421d9a-081a-4946-ad21-6eef126cda7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "6d421d9a-081a-4946-ad21-6eef126cda7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e537476a-75ce-4697-b4dd-000856455dc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 192,
    "yorig": 96
}