{
    "id": "1943bec0-bc09-4091-937d-60bb44f6d338",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeletonWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 24,
    "bbox_right": 167,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b393f098-1027-459e-968d-6d6c2c73bdf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "a63802e7-d33c-4a9f-a225-eaf4bf4f9eb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b393f098-1027-459e-968d-6d6c2c73bdf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "598084f2-e9ac-4dd2-93e5-ac800b2496ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b393f098-1027-459e-968d-6d6c2c73bdf9",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "2abcc1bb-9460-4ea7-a66e-1fc715808e2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "71ce7ad7-a689-44c4-9e1d-5bfe0967876c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2abcc1bb-9460-4ea7-a66e-1fc715808e2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0bc164e-ead2-4f77-87ef-27dcd4f24589",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2abcc1bb-9460-4ea7-a66e-1fc715808e2a",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "6bf512b7-59f4-4068-83f5-86d8ad54daae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "01f249e1-29e8-4f12-87b1-b8e95f2aaa66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bf512b7-59f4-4068-83f5-86d8ad54daae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d0110e6-a05b-4d8e-9f58-1230797f6531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bf512b7-59f4-4068-83f5-86d8ad54daae",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "705f9c46-1186-4837-b63a-a3fb5781e434",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "db8dc92b-9abb-4bd0-9a6a-d78a54e6844e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "705f9c46-1186-4837-b63a-a3fb5781e434",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1240d283-3b63-4754-835a-30b9a9ab5f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "705f9c46-1186-4837-b63a-a3fb5781e434",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "a61f72c5-c52c-4d8d-8c8a-69005bf373be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "d4ce6bb6-b2a6-484e-9230-5b111d978f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a61f72c5-c52c-4d8d-8c8a-69005bf373be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed3b28f-ba5d-4b1f-a1f6-44caaca231ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a61f72c5-c52c-4d8d-8c8a-69005bf373be",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "6202c797-f886-4cd5-83f6-6c98ef5e57f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "1f42b71c-6507-431b-87ec-27989113d966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6202c797-f886-4cd5-83f6-6c98ef5e57f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d7941bb-ec03-4358-8d3f-383e8d9eab5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6202c797-f886-4cd5-83f6-6c98ef5e57f1",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "c3a8238d-f7ae-4c17-b985-a9d437158ea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "4ac0d6df-d6d6-4bc3-a31c-326ef03e40fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3a8238d-f7ae-4c17-b985-a9d437158ea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "812f7a23-d11f-42b9-ad83-ff03f9cb07ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3a8238d-f7ae-4c17-b985-a9d437158ea0",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "7ea04ba5-fc84-45bb-9a0f-45f0c4f6483b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "c3cd6f0c-e5bb-4ce4-98b7-d86f40a55f58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ea04ba5-fc84-45bb-9a0f-45f0c4f6483b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63a97de4-098b-4379-af34-26e93d697fbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ea04ba5-fc84-45bb-9a0f-45f0c4f6483b",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "451521ba-e607-40ee-a047-2a4fe3882ed7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "36f0194f-3318-4fe8-8351-aed9f30f5175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "451521ba-e607-40ee-a047-2a4fe3882ed7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28b4879d-bb6d-417f-9054-ddb43f7b9d91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "451521ba-e607-40ee-a047-2a4fe3882ed7",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "e269c6d1-1042-4d5f-8a6f-9c295caf9a6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "8728ff0e-7902-413b-8da6-a65e4ff7bcec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e269c6d1-1042-4d5f-8a6f-9c295caf9a6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47e51dc3-1690-4cfd-ab25-2e7f1bc569b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e269c6d1-1042-4d5f-8a6f-9c295caf9a6b",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "345ab278-39d7-400f-92ba-6f2a1b193f58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "7110abf1-383d-4e90-aff4-9cdcc7063756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "345ab278-39d7-400f-92ba-6f2a1b193f58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac0ae344-117d-4c7f-9d59-18aa00c3caa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "345ab278-39d7-400f-92ba-6f2a1b193f58",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        },
        {
            "id": "82e7cc5e-bfe7-419e-b140-d6b6dd2a57e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "compositeImage": {
                "id": "ca955832-0bee-4850-9f10-5922025ed567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e7cc5e-bfe7-419e-b140-d6b6dd2a57e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39daac92-17c4-4b37-a40f-6b5520b744ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e7cc5e-bfe7-419e-b140-d6b6dd2a57e5",
                    "LayerId": "7e2c5386-e7b3-4238-89a4-38d1cff2598e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "7e2c5386-e7b3-4238-89a4-38d1cff2598e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1943bec0-bc09-4091-937d-60bb44f6d338",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}