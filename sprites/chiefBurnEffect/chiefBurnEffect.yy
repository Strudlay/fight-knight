{
    "id": "44a38497-797e-4a65-8ce0-75202b55efa6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "chiefBurnEffect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d472af3-0cac-4c46-877b-18dd9d490daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "92763968-7f80-434e-9446-edac17673cb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d472af3-0cac-4c46-877b-18dd9d490daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "895f2b4d-4d2c-476f-8d01-bbeb6e0c90cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d472af3-0cac-4c46-877b-18dd9d490daf",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        },
        {
            "id": "e13b6e95-e85f-4a7f-9639-8d8345aea8f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "d9b7c191-66bd-4379-8aae-6bea6641fa3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e13b6e95-e85f-4a7f-9639-8d8345aea8f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3592e9d8-57d1-4568-b740-8da32111f745",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e13b6e95-e85f-4a7f-9639-8d8345aea8f5",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        },
        {
            "id": "9a5b0686-8ac5-462b-9a4b-93d441d53752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "45a0cb7c-ac21-4b5c-9b24-bff7d0d3c51f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a5b0686-8ac5-462b-9a4b-93d441d53752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ee4948b-1396-4ddc-b3db-b82590972016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a5b0686-8ac5-462b-9a4b-93d441d53752",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        },
        {
            "id": "30c65909-d000-4db8-86d8-122b187b3088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "2faa912d-791d-4fc3-ad0e-9ea3b9af36b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30c65909-d000-4db8-86d8-122b187b3088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab9ead92-167d-4a04-81b0-efc46e70e5ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30c65909-d000-4db8-86d8-122b187b3088",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        },
        {
            "id": "204c60b0-d131-4dc2-9e20-4cfba1e85879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "ed0b8d45-74f6-4af3-9c1d-86aac7b65913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "204c60b0-d131-4dc2-9e20-4cfba1e85879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e454be0-c345-41f6-a815-d4ad1b68ea07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "204c60b0-d131-4dc2-9e20-4cfba1e85879",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        },
        {
            "id": "9baee98b-bf91-47fb-9cc0-e49c7bf6a625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "d6918501-b118-49df-975a-232362194139",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9baee98b-bf91-47fb-9cc0-e49c7bf6a625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a291cf8a-271c-4c11-b5e7-77964982357a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9baee98b-bf91-47fb-9cc0-e49c7bf6a625",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        },
        {
            "id": "4f24a7fc-6f84-4648-9edc-f2d531512777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "1248674e-8e4e-4c41-82f9-00738f41b28f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f24a7fc-6f84-4648-9edc-f2d531512777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1154fdca-f6d8-4904-b7d5-74a417f4fea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f24a7fc-6f84-4648-9edc-f2d531512777",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        },
        {
            "id": "20cbb043-cc9a-40ac-be25-4895f6e50e17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "d215044d-c57f-4947-85da-22570f865bb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20cbb043-cc9a-40ac-be25-4895f6e50e17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5457ee9b-4e58-4819-881e-bcabf9b31190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20cbb043-cc9a-40ac-be25-4895f6e50e17",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        },
        {
            "id": "492f3e9b-c9e6-4a99-9563-e2eb1e80557a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "9c90912e-311d-4b28-aa91-a83048c32d30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "492f3e9b-c9e6-4a99-9563-e2eb1e80557a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4dffb78-6e78-443a-8293-52e99966f2d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "492f3e9b-c9e6-4a99-9563-e2eb1e80557a",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        },
        {
            "id": "10ef922b-e20c-4a44-ab45-acb14eddcd2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "f36ba1eb-b17d-4764-9994-7001e20a5be2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10ef922b-e20c-4a44-ab45-acb14eddcd2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "912690b1-cbbe-4aaf-a53d-b583df94dae3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10ef922b-e20c-4a44-ab45-acb14eddcd2f",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        },
        {
            "id": "0bbc0327-c57a-4b5f-9cf0-25ed1f72d04a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "compositeImage": {
                "id": "ae988a1f-cf10-4b4b-b964-d85090f92eca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbc0327-c57a-4b5f-9cf0-25ed1f72d04a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f503693-a807-402d-a403-ccf7a1774d58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbc0327-c57a-4b5f-9cf0-25ed1f72d04a",
                    "LayerId": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "9dba4a20-a851-44ae-b0a5-d0c61d5cf552",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44a38497-797e-4a65-8ce0-75202b55efa6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}