{
    "id": "20b488eb-6c15-46a6-89c6-b51f0a376282",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robotPortrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 0,
    "bbox_right": 154,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1d34488-36c1-40c9-a7a0-841dc0864676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b488eb-6c15-46a6-89c6-b51f0a376282",
            "compositeImage": {
                "id": "67c50620-fbbb-4e65-91af-0a2ef5f30d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1d34488-36c1-40c9-a7a0-841dc0864676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9908fb75-e4f0-4d38-885c-21b9bff4bcd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1d34488-36c1-40c9-a7a0-841dc0864676",
                    "LayerId": "07427f7a-84ce-4d58-ad5e-80ab7ace65d5"
                }
            ]
        },
        {
            "id": "7eac7b49-8514-47f0-a0d3-12c8b41ab510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b488eb-6c15-46a6-89c6-b51f0a376282",
            "compositeImage": {
                "id": "323713ed-e537-44a5-8869-44757cd574ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eac7b49-8514-47f0-a0d3-12c8b41ab510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31d32f26-50da-4966-8188-b2cf061d2331",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eac7b49-8514-47f0-a0d3-12c8b41ab510",
                    "LayerId": "07427f7a-84ce-4d58-ad5e-80ab7ace65d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "07427f7a-84ce-4d58-ad5e-80ab7ace65d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20b488eb-6c15-46a6-89c6-b51f0a376282",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 155,
    "xorig": 85,
    "yorig": 58
}