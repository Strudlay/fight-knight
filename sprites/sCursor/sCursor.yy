{
    "id": "c5d7ba80-d358-4e7a-9199-3c88d46ee605",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 19,
    "bbox_right": 133,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db5b9a83-a9c9-4e77-ac2b-1cbc3ff6adea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d7ba80-d358-4e7a-9199-3c88d46ee605",
            "compositeImage": {
                "id": "6f2326a4-f936-42ab-8551-8da5fe9f7bc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db5b9a83-a9c9-4e77-ac2b-1cbc3ff6adea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c5519ad-83f8-427d-8c6b-c77ef2a0c890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db5b9a83-a9c9-4e77-ac2b-1cbc3ff6adea",
                    "LayerId": "4bd29116-1ff9-4492-9070-b4dab41dc229"
                }
            ]
        },
        {
            "id": "56f21192-436f-421b-9768-547d7d146e43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d7ba80-d358-4e7a-9199-3c88d46ee605",
            "compositeImage": {
                "id": "a6075ae8-13fb-460f-b3ee-d41324ea6220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f21192-436f-421b-9768-547d7d146e43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50968a5-05d1-4e50-b2cb-2380822d3504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f21192-436f-421b-9768-547d7d146e43",
                    "LayerId": "4bd29116-1ff9-4492-9070-b4dab41dc229"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "4bd29116-1ff9-4492-9070-b4dab41dc229",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5d7ba80-d358-4e7a-9199-3c88d46ee605",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 155,
    "xorig": 77,
    "yorig": 60
}