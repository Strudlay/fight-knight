{
    "id": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robotAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 187,
    "bbox_left": 40,
    "bbox_right": 375,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07d70417-2399-4fa2-96b0-8d3e6f94d06c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "23ed86be-18a4-42fb-9cc1-7fe8a0fc0ee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07d70417-2399-4fa2-96b0-8d3e6f94d06c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab2fe416-4373-4604-9d33-fc85eed9ed2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07d70417-2399-4fa2-96b0-8d3e6f94d06c",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "e7829daa-98eb-4612-88ec-11ba9680a08a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "5f239200-d833-42d4-84ac-69a8f76b94cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7829daa-98eb-4612-88ec-11ba9680a08a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b7337f-74e2-4566-a690-35fab7344194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7829daa-98eb-4612-88ec-11ba9680a08a",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "97c6da0d-5ed4-44f5-8a9f-02a57d8dd510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "0e80a9cc-7e6a-47bf-85e0-4e26f40565cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97c6da0d-5ed4-44f5-8a9f-02a57d8dd510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0f9492c-afa8-44f8-b41e-7ca4676a827e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c6da0d-5ed4-44f5-8a9f-02a57d8dd510",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "296c1533-eb18-4601-a626-03d45bc83cdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "96d443fd-b182-45a0-b620-0a2d433a3761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "296c1533-eb18-4601-a626-03d45bc83cdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47fb98c2-57c8-4162-91a0-b1b6e05d295d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "296c1533-eb18-4601-a626-03d45bc83cdb",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "f1b197e6-dee5-4727-8a00-e9ed81b8019c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "090ebc07-99b7-466f-a9a0-0ae859e22354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1b197e6-dee5-4727-8a00-e9ed81b8019c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25cd60a3-2c16-40d2-bb7f-b8fdac85144b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1b197e6-dee5-4727-8a00-e9ed81b8019c",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "161cc948-ae99-4e03-abb8-5da55ccb7250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "3871083f-b86f-4a81-8b26-56f67e6d5c47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "161cc948-ae99-4e03-abb8-5da55ccb7250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2a920a4-f636-4e67-9d93-e746a2471f64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "161cc948-ae99-4e03-abb8-5da55ccb7250",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "ec8ef40c-faf9-4363-ab98-716dbbe59fa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "651c3171-94b2-4a95-9d23-46c074a2eb9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec8ef40c-faf9-4363-ab98-716dbbe59fa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750749f5-30aa-4fa9-a6c1-d59e40b4839b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec8ef40c-faf9-4363-ab98-716dbbe59fa2",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "86689d33-cb98-48dd-8753-602fd5fdad29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "c772ee45-734c-4626-aa3b-c0f64afa1241",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86689d33-cb98-48dd-8753-602fd5fdad29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e74758f6-c836-49c9-9a80-61ad4eb97a8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86689d33-cb98-48dd-8753-602fd5fdad29",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "dcc3f08f-8ecf-494b-8c35-f377f1b1d9ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "cffd5029-9331-4d93-b818-5428924aee19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcc3f08f-8ecf-494b-8c35-f377f1b1d9ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "330d8123-b04f-4359-b9ba-39f4465aba40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcc3f08f-8ecf-494b-8c35-f377f1b1d9ab",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "3d0dea62-7918-44e2-b6ac-893389bd8b19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "f13f283c-9d71-41c5-b83d-43c9c328fae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d0dea62-7918-44e2-b6ac-893389bd8b19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07c719a8-f2ea-4e79-bb35-663dbae1b5ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d0dea62-7918-44e2-b6ac-893389bd8b19",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "b89bd7f1-c030-490f-8608-6e55af78f37c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "ab298f88-d216-4b87-b4c0-361dbfe0b298",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b89bd7f1-c030-490f-8608-6e55af78f37c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e7e963b-3d51-4dfc-be40-7003f0533931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b89bd7f1-c030-490f-8608-6e55af78f37c",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "c26c30d6-63a3-4e5b-8533-f48b804396fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "89ea1470-ad45-4035-805c-7c217e256b6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c26c30d6-63a3-4e5b-8533-f48b804396fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6a60d61-3af3-4ae3-a64d-f86e831dc481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c26c30d6-63a3-4e5b-8533-f48b804396fe",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "942be10a-5484-43f4-8d7b-76d0dcad5d02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "f7862e3b-2dfc-470e-b74e-f0f9cb15a515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "942be10a-5484-43f4-8d7b-76d0dcad5d02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aea1609-2698-4dab-bc3f-7dec7790f7f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "942be10a-5484-43f4-8d7b-76d0dcad5d02",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "12996a4e-8d82-4b32-8a14-24b60392fd70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "db1e56b0-0818-4db9-b2b3-b01fcdf86e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12996a4e-8d82-4b32-8a14-24b60392fd70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7756397-2b33-4a1d-904f-db2d076f581c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12996a4e-8d82-4b32-8a14-24b60392fd70",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "10203702-44ac-410e-8750-747cb120638e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "e0513628-0e58-4200-9e78-e37b63a4c93f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10203702-44ac-410e-8750-747cb120638e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50cc7ef5-a1ac-486d-b122-c023256317ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10203702-44ac-410e-8750-747cb120638e",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "eb7f6a28-ac5d-4917-b668-cf33c2867ea2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "3f845f68-d1b4-4676-ac27-5186a6fc2d5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb7f6a28-ac5d-4917-b668-cf33c2867ea2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29c78807-b53a-4023-bf74-79ec147b81f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb7f6a28-ac5d-4917-b668-cf33c2867ea2",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        },
        {
            "id": "8cafc72e-117e-4870-a06c-439209786a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "compositeImage": {
                "id": "f14668c0-d60c-4b16-aa62-a1f2b7410760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cafc72e-117e-4870-a06c-439209786a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e07652e9-995c-48ef-9c0b-f368ac8b8e4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cafc72e-117e-4870-a06c-439209786a7c",
                    "LayerId": "d1c75f6e-1f23-45ef-8470-f8c515954891"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "d1c75f6e-1f23-45ef-8470-f8c515954891",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f081454d-2823-421a-88f9-9ae5bc9b9b07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 192,
    "yorig": 96
}