{
    "id": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "archerWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 36,
    "bbox_right": 151,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1adaa63e-6b3b-4454-a902-090532e9faa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "9a2f0abd-d49e-4f57-9ed0-2089e9f5224c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1adaa63e-6b3b-4454-a902-090532e9faa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f59ede88-5455-4c64-8e62-86bf80e838f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1adaa63e-6b3b-4454-a902-090532e9faa4",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "1a1e0d8f-c594-4e02-9634-e88c8262733a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "cde8ae3d-bb38-4703-9d10-1483a11249b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a1e0d8f-c594-4e02-9634-e88c8262733a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba1b7fef-20e8-4bb6-b3be-e88bb1849aa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a1e0d8f-c594-4e02-9634-e88c8262733a",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "f4dccd27-52fe-4429-9b14-7f6d9d0313f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "96edfdf9-f022-4186-83c8-853d101f1bbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4dccd27-52fe-4429-9b14-7f6d9d0313f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "237b9beb-0852-41ae-910f-036733344097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4dccd27-52fe-4429-9b14-7f6d9d0313f6",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "8fb42b7b-70fa-4460-90ff-4786b9a9eff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "d026d61d-76a7-4463-883d-0da165a6ba0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fb42b7b-70fa-4460-90ff-4786b9a9eff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42c36e91-cc18-445a-8c97-9cb8e7715a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fb42b7b-70fa-4460-90ff-4786b9a9eff0",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "21d782bf-a2b4-4920-8d04-231e97504dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "c3cf1488-0b51-4555-a985-1be57c595bb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21d782bf-a2b4-4920-8d04-231e97504dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6c54db2-1e5d-445c-879a-75e6c189ea0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21d782bf-a2b4-4920-8d04-231e97504dbd",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "f52a194f-119a-4409-97ae-f632186e3233",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "d8ba730b-fd54-4c2e-8061-9f0e7fbd7845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f52a194f-119a-4409-97ae-f632186e3233",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf120d93-3182-46c9-a706-48da40e85fc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f52a194f-119a-4409-97ae-f632186e3233",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "6e740bf8-07e1-4980-9e17-c4fd91b54ac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "1570b1c4-d3f4-45a5-bfab-ddc8edc049d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e740bf8-07e1-4980-9e17-c4fd91b54ac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ba8e8a7-f46c-48f4-85d0-ae24ec387e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e740bf8-07e1-4980-9e17-c4fd91b54ac3",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "96cd63eb-fa10-4fce-8617-5dc3cae78ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "3e43218c-62cb-4d6b-9418-2377468b4b45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96cd63eb-fa10-4fce-8617-5dc3cae78ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebc57c0d-04f6-4240-b408-9a0bb6152a06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96cd63eb-fa10-4fce-8617-5dc3cae78ad6",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "9f3c4d74-3d20-4fc8-a725-57847eda3e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "3be0fff9-a044-4695-b667-3b688acd1f87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f3c4d74-3d20-4fc8-a725-57847eda3e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a9e8070-2969-4d67-be84-751aa8d01e5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f3c4d74-3d20-4fc8-a725-57847eda3e3c",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "d88d120d-7e32-4aeb-8ebf-e07ff2bc4b6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "fb6e7736-1265-4b6b-a30b-05e0ae977ac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d88d120d-7e32-4aeb-8ebf-e07ff2bc4b6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fca33fa1-1c74-4b77-94d7-dfaa8ad5aaa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d88d120d-7e32-4aeb-8ebf-e07ff2bc4b6c",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "61c5269e-9f37-4ae5-80f2-27d092741ffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "3699c7cc-3b03-4f28-a32b-75e61e37f7d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61c5269e-9f37-4ae5-80f2-27d092741ffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ff7f00a-997c-4222-99cc-23f9ed604069",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61c5269e-9f37-4ae5-80f2-27d092741ffb",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        },
        {
            "id": "c2386b3e-f55e-403f-8f62-bd268d4cdc10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "compositeImage": {
                "id": "54d2b98c-3c94-4471-9877-080f8f02ba0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2386b3e-f55e-403f-8f62-bd268d4cdc10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84535d33-7029-4fe3-824f-9b58f4bd8756",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2386b3e-f55e-403f-8f62-bd268d4cdc10",
                    "LayerId": "719d63d7-a9fc-45a6-882f-1ad5089475da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "719d63d7-a9fc-45a6-882f-1ad5089475da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53aea4d1-c749-42c0-bf3f-837e723d1f4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}