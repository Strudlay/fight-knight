{
    "id": "d9b16bc3-ccb9-4516-b479-672472edd76e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_pillars_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 183,
    "bbox_left": 56,
    "bbox_right": 335,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8a78fb7-0654-4cac-92e7-84ed97ba8cbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9b16bc3-ccb9-4516-b479-672472edd76e",
            "compositeImage": {
                "id": "a8c0cd05-c5b0-4482-b18b-7d8b20575444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8a78fb7-0654-4cac-92e7-84ed97ba8cbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0ff5442-db23-4e01-8257-f1aff6fd138d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8a78fb7-0654-4cac-92e7-84ed97ba8cbe",
                    "LayerId": "927f0103-2477-4cd7-a9d8-23f8f4b2ca7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 184,
    "layers": [
        {
            "id": "927f0103-2477-4cd7-a9d8-23f8f4b2ca7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9b16bc3-ccb9-4516-b479-672472edd76e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 336,
    "xorig": 0,
    "yorig": 0
}