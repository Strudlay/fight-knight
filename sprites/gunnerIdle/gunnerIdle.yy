{
    "id": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gunnerIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 16,
    "bbox_right": 135,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77716644-b260-46c7-b4f6-de2cd69d4169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "eb7eec23-f389-46eb-a741-1f17dcf568a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77716644-b260-46c7-b4f6-de2cd69d4169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d6ce83-f9a1-451a-be82-34e53766989c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77716644-b260-46c7-b4f6-de2cd69d4169",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "6c44efba-dfe0-4fa1-a58d-d73e35a64145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "18c99fab-2da5-413d-9303-42e42eb4af7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c44efba-dfe0-4fa1-a58d-d73e35a64145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "344bb4ac-2a69-4939-9c47-fca9647ab1c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c44efba-dfe0-4fa1-a58d-d73e35a64145",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "3599f731-f22c-46e8-b4f0-6a03d7bda04b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "7cc69c12-0bf6-4e51-be9e-21380fc3c77d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3599f731-f22c-46e8-b4f0-6a03d7bda04b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "165d88c7-969f-45fa-9114-65d6aa21e914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3599f731-f22c-46e8-b4f0-6a03d7bda04b",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "1cfaa112-5c94-40dc-a1a1-3874549b0090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "ae3c9c03-6d7e-400f-97f3-c01d0f78a7de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cfaa112-5c94-40dc-a1a1-3874549b0090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebf91ed9-5ca3-45c0-9bd8-9cac7104bd7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cfaa112-5c94-40dc-a1a1-3874549b0090",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "d16004ad-91b1-4ee0-b9f8-f3adb0426126",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "8ea351d7-7fdf-4632-b13d-4f444ecc06e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d16004ad-91b1-4ee0-b9f8-f3adb0426126",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fceaff91-1fb2-4fa0-87d7-783c9b14df2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d16004ad-91b1-4ee0-b9f8-f3adb0426126",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "a51e5579-a3c0-45d5-b32b-67fc15e852f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "1051bd03-ff5a-4f65-ab8c-b48abb2c5ec6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a51e5579-a3c0-45d5-b32b-67fc15e852f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6caa78c-038f-4bfd-9bbb-b3aa3ee0cd78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a51e5579-a3c0-45d5-b32b-67fc15e852f0",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "cf3af838-a82e-456c-af62-e3ea6b927a00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "41e5ebfb-bf09-4ab5-8c64-4764b2d45e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf3af838-a82e-456c-af62-e3ea6b927a00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04013a50-83fa-4715-8ce9-d141f5f1df9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf3af838-a82e-456c-af62-e3ea6b927a00",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "80b11205-6b20-4b6f-ab7c-c07cb6d1f086",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "7e8797a5-8a76-48fb-85af-866772e9f3b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b11205-6b20-4b6f-ab7c-c07cb6d1f086",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1217157b-a521-441d-8ff3-5a5e7dce68bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b11205-6b20-4b6f-ab7c-c07cb6d1f086",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "e4db9f38-a23b-4ce0-8d36-9c4fe4cfc9b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "c7917658-5b3d-421b-89a1-34228ff2f04c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4db9f38-a23b-4ce0-8d36-9c4fe4cfc9b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "754a417a-e645-478e-b801-d0e89ea7a7fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4db9f38-a23b-4ce0-8d36-9c4fe4cfc9b1",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "433db377-293f-4e10-b6f9-1c2325c12adc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "f97d3862-9a32-474f-a4a5-fe81b5f9d1aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "433db377-293f-4e10-b6f9-1c2325c12adc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e82abd5b-184e-401d-b863-ee07d6de9f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "433db377-293f-4e10-b6f9-1c2325c12adc",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "79824500-2c5e-4df0-8e22-007466c9b7a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "d3427d42-bce8-4a8f-ac11-9f238a0fe419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79824500-2c5e-4df0-8e22-007466c9b7a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f569ce-3a1f-4c7a-a834-33e5cacc200f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79824500-2c5e-4df0-8e22-007466c9b7a2",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "f22bcbdd-5abd-4332-97dd-35d6fd2571c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "f65ab55b-aafd-4a2f-b81c-f32e3c713f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f22bcbdd-5abd-4332-97dd-35d6fd2571c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb5e6815-814c-4773-8303-96116917f2d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f22bcbdd-5abd-4332-97dd-35d6fd2571c7",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "0358906d-4971-4242-bd44-13262f7184a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "42d05287-4f5c-406a-a008-1f0f2f0b9275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0358906d-4971-4242-bd44-13262f7184a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "422c722e-6939-4c89-b411-bc9a101b8ee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0358906d-4971-4242-bd44-13262f7184a4",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "f4eb78c0-9eb5-4147-b1a2-06833c3e6680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "33175e44-81d8-4f22-836b-6fbea7af2dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4eb78c0-9eb5-4147-b1a2-06833c3e6680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07858f61-6b13-4fb0-bcf1-a2146d0c180a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4eb78c0-9eb5-4147-b1a2-06833c3e6680",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "bf603d77-29c7-4b3a-98da-55b184abf2df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "2b4cde35-b704-46f2-b750-0fa37eb9f344",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf603d77-29c7-4b3a-98da-55b184abf2df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "896584a0-d894-4702-b11e-ead4e35e3608",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf603d77-29c7-4b3a-98da-55b184abf2df",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "5616409e-599b-4eb3-9939-ce6e2030d6ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "a795a247-2637-4635-9467-989e77bb94d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5616409e-599b-4eb3-9939-ce6e2030d6ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c06c7f-8536-4597-8df1-2a92e6f30fba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5616409e-599b-4eb3-9939-ce6e2030d6ac",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "25c3413a-ffbc-4e71-b49c-3041d5b8c397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "a292c616-4e51-4cf2-8170-3de6a6c2d855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25c3413a-ffbc-4e71-b49c-3041d5b8c397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e43ba29c-9e0b-4887-9301-5e8739180b77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25c3413a-ffbc-4e71-b49c-3041d5b8c397",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "6f8a6ac6-568a-4e77-b69e-f17410a18dc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "6eaf121b-a62c-436b-b55c-fd636de65f20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f8a6ac6-568a-4e77-b69e-f17410a18dc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eeb52a3-9b22-4f43-8200-61b4b3a9f8ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f8a6ac6-568a-4e77-b69e-f17410a18dc1",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "42df6e75-c746-43aa-9567-514041ce75d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "b7b525e6-162f-45c5-be44-6202df24ae3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42df6e75-c746-43aa-9567-514041ce75d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "391ce28a-6048-4da4-9d4a-4b2bbf5eb101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42df6e75-c746-43aa-9567-514041ce75d6",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "17e6209b-2626-4d56-a460-6ce1ad57f990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "5a2f9371-cf0e-4b0b-b744-7655859d0b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e6209b-2626-4d56-a460-6ce1ad57f990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a43fcdd-058c-4d31-a4cd-b54282685f5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e6209b-2626-4d56-a460-6ce1ad57f990",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "0fe34b8e-fcab-4ae9-9e94-2bb107cd2307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "98f9d196-1a5f-42bd-adc9-b6ba76b9c108",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe34b8e-fcab-4ae9-9e94-2bb107cd2307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4062ca94-3698-4c5c-afe1-3fccaf60352f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe34b8e-fcab-4ae9-9e94-2bb107cd2307",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        },
        {
            "id": "804aed45-d25a-4801-9897-f0047b770b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "compositeImage": {
                "id": "88c9932a-d666-4f57-87ae-f2c9d1cf13c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "804aed45-d25a-4801-9897-f0047b770b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c94da45b-3ca7-47f6-b439-ad7d0200c41e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "804aed45-d25a-4801-9897-f0047b770b58",
                    "LayerId": "190157a7-007e-4369-bf94-2fdfff93cb2e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "190157a7-007e-4369-bf94-2fdfff93cb2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ce405c8-7057-45c5-b57f-fa89ccb27e26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}