{
    "id": "a5ed5bef-dc59-4097-b700-0f7e011f83df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 76,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a0df975-19fd-4a90-8849-6775d9ff1720",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5ed5bef-dc59-4097-b700-0f7e011f83df",
            "compositeImage": {
                "id": "4a13cdd8-a24c-4e4f-9a48-854594455f7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a0df975-19fd-4a90-8849-6775d9ff1720",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b73a1958-1b57-449f-be47-50e0b76fc7c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a0df975-19fd-4a90-8849-6775d9ff1720",
                    "LayerId": "bc1b7d8b-22b8-4a12-8680-5f4d0afa1804"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "bc1b7d8b-22b8-4a12-8680-5f4d0afa1804",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5ed5bef-dc59-4097-b700-0f7e011f83df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 38,
    "yorig": 30
}