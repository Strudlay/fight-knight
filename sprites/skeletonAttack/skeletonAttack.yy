{
    "id": "e5459e9e-38f8-45c7-8f57-69599aab3964",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeletonAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 287,
    "bbox_left": 4,
    "bbox_right": 323,
    "bbox_top": 72,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42acf0c1-9a97-4bed-a929-126b1e50e6be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "65f493d1-aac7-459a-8289-a4d305465987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42acf0c1-9a97-4bed-a929-126b1e50e6be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a566d4b-3b72-4678-88c1-78fbb130fde2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42acf0c1-9a97-4bed-a929-126b1e50e6be",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "3948b1c9-d590-4562-ae99-eba964eb6168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "cafb9e63-0f8e-4339-9740-39f9155711f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3948b1c9-d590-4562-ae99-eba964eb6168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9ee7911-011b-465d-bc29-eb75ab2acd64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3948b1c9-d590-4562-ae99-eba964eb6168",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "4679be1e-2cdd-4434-9d28-81f81b489c4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "6802c6c5-3977-4305-b6f0-dcb0315ee3f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4679be1e-2cdd-4434-9d28-81f81b489c4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7946673-982f-4b0a-9c46-8b7134f14ba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4679be1e-2cdd-4434-9d28-81f81b489c4f",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "c2e785c6-d52f-4be8-a52f-e0890864cebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "5854a519-9332-4679-b682-155786b2bf47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2e785c6-d52f-4be8-a52f-e0890864cebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab558dfd-3232-4c01-9233-b9acb068e980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2e785c6-d52f-4be8-a52f-e0890864cebb",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "18f0de46-7b7d-4b11-bec3-dac16db50421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "331db984-5832-45e0-bd52-46d0f4e57c89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f0de46-7b7d-4b11-bec3-dac16db50421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d71cc6d-358e-4267-8949-fffbc83ea2eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f0de46-7b7d-4b11-bec3-dac16db50421",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "f3d543d3-87a5-41e0-92e6-0c30d5031612",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "270179aa-60af-4518-ae1a-d17bd26d59d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3d543d3-87a5-41e0-92e6-0c30d5031612",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c5ac262-1895-4331-9565-b51a4274e1f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3d543d3-87a5-41e0-92e6-0c30d5031612",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "36f0ea27-df8b-459d-ae66-8df2d006f4f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "7702f753-0ba0-416d-ab33-594d48d0c6be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36f0ea27-df8b-459d-ae66-8df2d006f4f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b91bc33e-9eb1-476d-99a2-048dd30cc1fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36f0ea27-df8b-459d-ae66-8df2d006f4f5",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "57b1855a-41a1-4c84-af3a-a1fa9e14913d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "960cd438-a7b4-43ed-9635-a4135d9c7baf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57b1855a-41a1-4c84-af3a-a1fa9e14913d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81ef70a6-d071-4ea1-8c82-9ed411c01953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57b1855a-41a1-4c84-af3a-a1fa9e14913d",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "6334af21-cc35-47cb-9708-4beb9d86a3ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "f250e753-5a52-4c4b-afe3-cf45798f2ced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6334af21-cc35-47cb-9708-4beb9d86a3ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54d6c4af-3911-4439-9b5e-4307d873c012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6334af21-cc35-47cb-9708-4beb9d86a3ff",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "b277ea71-a59d-4d61-baee-07eb347bb4e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "54b7951b-ee6e-4279-b476-775a4c954dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b277ea71-a59d-4d61-baee-07eb347bb4e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8493ca8-d7dd-46bd-ba5e-c78da5ff4e9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b277ea71-a59d-4d61-baee-07eb347bb4e0",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "8425a3c4-04aa-4d18-b409-c2a2f6e59c4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "1568f5a8-d729-44ba-8998-4df942613d58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8425a3c4-04aa-4d18-b409-c2a2f6e59c4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e6d4113-4a25-42a5-ad3d-26046220b394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8425a3c4-04aa-4d18-b409-c2a2f6e59c4d",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "342f912e-3047-41a4-a8d0-7ebc9bf19844",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "64e689d5-76f1-4af3-9923-d4856984bff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "342f912e-3047-41a4-a8d0-7ebc9bf19844",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a014c0a-65eb-4297-a978-cf347284b1d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "342f912e-3047-41a4-a8d0-7ebc9bf19844",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "bb2e1b13-9bb7-4957-8976-6ba0ea5a8ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "d108c7d8-0cd6-4382-a963-a6e20f90ff8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb2e1b13-9bb7-4957-8976-6ba0ea5a8ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ee88dca-7658-4ef3-a5c0-a17a4f652764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb2e1b13-9bb7-4957-8976-6ba0ea5a8ced",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        },
        {
            "id": "40a3de52-cd7c-4fe3-bc88-58c6260a1666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "compositeImage": {
                "id": "88bc1307-1482-432a-bc13-faacaefd1062",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40a3de52-cd7c-4fe3-bc88-58c6260a1666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6453d8d0-842b-46f3-bc01-3c02b14b2a23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40a3de52-cd7c-4fe3-bc88-58c6260a1666",
                    "LayerId": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "ba4f22f8-2da1-4a00-8694-00b4e4b95f74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5459e9e-38f8-45c7-8f57-69599aab3964",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 192,
    "yorig": 144
}