{
    "id": "17ea2de0-3361-40d6-8e88-f777a0f8a030",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "archerPortrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 0,
    "bbox_right": 154,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "158b8a30-d8eb-49f4-8955-630b9bf2bad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17ea2de0-3361-40d6-8e88-f777a0f8a030",
            "compositeImage": {
                "id": "2ae27a15-fe77-40e0-b4a6-b62e7f86b53e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "158b8a30-d8eb-49f4-8955-630b9bf2bad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c75aa48-889c-4fc2-9b8b-8356355826cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158b8a30-d8eb-49f4-8955-630b9bf2bad5",
                    "LayerId": "3449850e-4169-42aa-bce4-36f58ef91495"
                }
            ]
        },
        {
            "id": "ec198ed5-9910-472f-8648-fe0fee81a795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17ea2de0-3361-40d6-8e88-f777a0f8a030",
            "compositeImage": {
                "id": "8d6e3905-b35b-42d0-8641-02524bc0b119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec198ed5-9910-472f-8648-fe0fee81a795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2491d748-fc3d-44fa-b003-db842a62a6c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec198ed5-9910-472f-8648-fe0fee81a795",
                    "LayerId": "3449850e-4169-42aa-bce4-36f58ef91495"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "3449850e-4169-42aa-bce4-36f58ef91495",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17ea2de0-3361-40d6-8e88-f777a0f8a030",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 155,
    "xorig": 70,
    "yorig": 58
}