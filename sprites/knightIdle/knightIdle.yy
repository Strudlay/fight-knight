{
    "id": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "knightIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 16,
    "bbox_right": 179,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6aa19e71-a664-4232-8773-720bcc66ea13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "880344fd-0e98-43e8-9b73-88d611d8f965",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aa19e71-a664-4232-8773-720bcc66ea13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c86b5f8-596a-47ab-8be7-8b6158a03355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aa19e71-a664-4232-8773-720bcc66ea13",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "7ec12ca3-08e5-400a-a672-c7125ca29193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "09a681e4-3d53-4b9e-a3ad-1e5c32108287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ec12ca3-08e5-400a-a672-c7125ca29193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8a3d605-d0ab-48f0-b67d-d4cd9edac9cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ec12ca3-08e5-400a-a672-c7125ca29193",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "66b8426d-06da-479b-ad74-5ac49048908a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "05297b53-b68a-4e9e-9032-cb9eaed664c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66b8426d-06da-479b-ad74-5ac49048908a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42bb7137-5ed0-41c9-b522-3f8dc8808a94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66b8426d-06da-479b-ad74-5ac49048908a",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "693fdc64-bad7-4df1-a687-a09492c1e758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "8f201b3f-c9e0-4242-9820-d622e1971bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "693fdc64-bad7-4df1-a687-a09492c1e758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c15b4144-82bf-42e4-aaf0-ae7ccf45d374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "693fdc64-bad7-4df1-a687-a09492c1e758",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "13d5366b-6abf-4d70-ad87-7be2117ed229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "be884434-21b4-45ab-be1d-72eb4b864a0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d5366b-6abf-4d70-ad87-7be2117ed229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1f05b11-4d6f-4ee4-8fa7-1887ea032d8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d5366b-6abf-4d70-ad87-7be2117ed229",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "f2fd8f6b-6d3c-4d66-a02e-881e11b4cf6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "808136e3-f12e-4e7d-92fe-fceefdb783b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2fd8f6b-6d3c-4d66-a02e-881e11b4cf6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2321969c-0ecf-4ecf-b3c0-6b2369f918eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2fd8f6b-6d3c-4d66-a02e-881e11b4cf6a",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "2cf32c47-79e5-4adc-9219-3b5bc07782bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "66ada8bb-b575-47b2-9940-2a80f4e5f191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cf32c47-79e5-4adc-9219-3b5bc07782bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31b123d0-cdee-4058-a511-2d2e30b75f81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cf32c47-79e5-4adc-9219-3b5bc07782bf",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "3c489158-e3d7-4920-8828-b6f1fac74452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "80e82e8d-b4f4-4cb4-8f20-a0e805dc9f30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c489158-e3d7-4920-8828-b6f1fac74452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad133304-1f99-4fbf-8aee-4d2ea8119061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c489158-e3d7-4920-8828-b6f1fac74452",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "c68e0594-da88-48ac-9b65-0a767f1984f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "604ee3fd-e57c-46e5-96e6-5e772f5af970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c68e0594-da88-48ac-9b65-0a767f1984f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea3d0f6e-7b09-4750-b0d8-9d1e4a459975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c68e0594-da88-48ac-9b65-0a767f1984f3",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "9620c58d-945d-4eb7-8c82-4eb009593ac0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "04a8dc87-4e49-49c0-b536-9863f0d4b9fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9620c58d-945d-4eb7-8c82-4eb009593ac0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df1ea851-55ea-4bae-b41f-38e28c158deb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9620c58d-945d-4eb7-8c82-4eb009593ac0",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "92a4ddb2-b9c4-484b-a38d-61df9e08473a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "c5db4a9d-766a-478c-877d-5fb8134af678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92a4ddb2-b9c4-484b-a38d-61df9e08473a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda3a23d-66f7-4621-8557-6e62a7f27103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92a4ddb2-b9c4-484b-a38d-61df9e08473a",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "54a6749c-d1e5-4739-bb27-fe605bc11d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "f38a1cda-e384-490d-8f30-eb18454d0d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54a6749c-d1e5-4739-bb27-fe605bc11d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db76efd9-88d6-453a-885f-85deb693971d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54a6749c-d1e5-4739-bb27-fe605bc11d3e",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "a49fc09c-6ee4-45cb-882a-c49598b1a742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "254dd754-d4c6-4e7c-8b50-bc7544cd6356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a49fc09c-6ee4-45cb-882a-c49598b1a742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "059333cd-30ec-4d75-b759-d9cafc858bdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a49fc09c-6ee4-45cb-882a-c49598b1a742",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        },
        {
            "id": "356a2ea7-7c6f-423e-be4a-c7aec818dffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "compositeImage": {
                "id": "9e680b0e-702c-4a97-be54-9ac1185a8ee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "356a2ea7-7c6f-423e-be4a-c7aec818dffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b813e1e2-63ed-4082-9c35-ab862cbe1bf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "356a2ea7-7c6f-423e-be4a-c7aec818dffc",
                    "LayerId": "be890eb3-f8a6-4320-bbb5-17f5e26015a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "be890eb3-f8a6-4320-bbb5-17f5e26015a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78e3a77c-3d20-4736-9b3a-25ce22af1a6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 70,
    "yorig": 95
}