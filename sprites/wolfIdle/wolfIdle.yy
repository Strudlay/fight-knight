{
    "id": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wolfIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 171,
    "bbox_left": 8,
    "bbox_right": 191,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6210ba82-e5b6-4476-89ef-528a73431274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "09eee607-9b08-446a-a2f2-cb34b0a2b9a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6210ba82-e5b6-4476-89ef-528a73431274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baff33de-881d-47e2-91b2-e384d3803b02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6210ba82-e5b6-4476-89ef-528a73431274",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "7c667205-589e-40dc-ba31-d8dd4d5b52fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "cf9e80bb-3c3e-4df5-9b27-82e7c06a849e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c667205-589e-40dc-ba31-d8dd4d5b52fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f323c35c-71bc-427f-87f6-b996b1ef6bae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c667205-589e-40dc-ba31-d8dd4d5b52fe",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "5d6b3109-ed22-43ea-9d32-520ffa46f252",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "d3c56d25-58e4-46a4-b7a8-313c446ffbbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d6b3109-ed22-43ea-9d32-520ffa46f252",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91c4f846-d12a-4ac1-8974-0d98c1a4fca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d6b3109-ed22-43ea-9d32-520ffa46f252",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "3226aed5-9a8c-4f13-8fe1-f8b51b32fbd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "21996941-3342-4592-8f95-e1e1573382bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3226aed5-9a8c-4f13-8fe1-f8b51b32fbd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1048814d-1e49-47b1-b0d5-dee917466a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3226aed5-9a8c-4f13-8fe1-f8b51b32fbd2",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "bd134105-f5ae-482a-8a30-86289bb8ad24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "9829c639-9c8d-4c47-9c37-4f47ee5fd395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd134105-f5ae-482a-8a30-86289bb8ad24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b25b9eb7-a62c-4d44-892c-5a119893b7d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd134105-f5ae-482a-8a30-86289bb8ad24",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "9c219f00-6be8-4fa2-849e-4a777caf3b7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "b88ef3d9-519e-4290-8552-33267b497497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c219f00-6be8-4fa2-849e-4a777caf3b7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b453f76-0258-42ff-8a9b-1ed5ed8c3e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c219f00-6be8-4fa2-849e-4a777caf3b7b",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "265098fa-a742-47e5-a4de-9e0e4aebf389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "cb4782c5-67b4-4010-a0a7-c8e425a55516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "265098fa-a742-47e5-a4de-9e0e4aebf389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1567b3e4-8045-40ed-acaf-4023115d34b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "265098fa-a742-47e5-a4de-9e0e4aebf389",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "93e8162d-6d18-4003-bd88-9c81bc8aa1c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "7ce19605-3f73-44b4-aad7-874158ca9642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e8162d-6d18-4003-bd88-9c81bc8aa1c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6b30bf5-78c4-46f5-a4d9-afa5474a3297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e8162d-6d18-4003-bd88-9c81bc8aa1c7",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "44d9ddb2-e08b-4900-bb2c-28df78b4c9b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "77953aba-1e0f-4b24-af7a-7c685b7b411e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44d9ddb2-e08b-4900-bb2c-28df78b4c9b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d3b46b-bbb4-4446-a3c8-8fc3f62ef1c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44d9ddb2-e08b-4900-bb2c-28df78b4c9b5",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "212d40b8-8160-47d6-bbc6-8e9cd0ffb827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "2a6bcf70-66e8-4a6e-9cc7-5cd2e7c766f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "212d40b8-8160-47d6-bbc6-8e9cd0ffb827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8fcbba4-2d39-4c5e-8512-08c204fadaa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "212d40b8-8160-47d6-bbc6-8e9cd0ffb827",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "5ac33647-d493-4def-9449-3fcdeaefc43e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "3c3f847d-3d53-4639-8ce2-04c73d148154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ac33647-d493-4def-9449-3fcdeaefc43e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3bae992-7d76-4c2e-bf86-0784d14b5ba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ac33647-d493-4def-9449-3fcdeaefc43e",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        },
        {
            "id": "68c9b554-44ef-4408-8be9-8ac7f1622abd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "compositeImage": {
                "id": "7b1b5ef0-7e8b-4a03-8518-90692c48196e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68c9b554-44ef-4408-8be9-8ac7f1622abd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdf2330e-2ad9-4ff2-82c4-3d868060c07d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68c9b554-44ef-4408-8be9-8ac7f1622abd",
                    "LayerId": "21cb1cf3-fc43-4883-b990-a1198b6266a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "21cb1cf3-fc43-4883-b990-a1198b6266a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae87d2b7-3d1d-45e6-bde1-5a89534b295a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}