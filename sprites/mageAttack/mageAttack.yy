{
    "id": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mageAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 92,
    "bbox_right": 375,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dd5596a-ebd9-4eda-b815-a929a7dc1261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "b7842c98-b747-478f-8a24-1ed5643403a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dd5596a-ebd9-4eda-b815-a929a7dc1261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4a224e7-bbd4-4a5b-9d2f-972dfa2ee396",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dd5596a-ebd9-4eda-b815-a929a7dc1261",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "00f2a6f9-7d8f-4e34-a780-c9ab1e0367c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "0c1c8dad-b34d-4bf8-8c22-fb68edfdfb96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00f2a6f9-7d8f-4e34-a780-c9ab1e0367c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4e6b350-e14b-41f8-8104-ea78265c07ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00f2a6f9-7d8f-4e34-a780-c9ab1e0367c5",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "baff5b0f-e14d-4c41-aa2e-6533f3e0f197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "a7a85bb6-b57e-4276-a6cc-c72af604c1c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baff5b0f-e14d-4c41-aa2e-6533f3e0f197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d80cf84-6bc6-4ab3-9e4c-a7c9a8ab892d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baff5b0f-e14d-4c41-aa2e-6533f3e0f197",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "95b40147-7d92-4d5f-9d2a-2fb1d5c40441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "58a9aab8-65ee-46cc-bde8-7b07d6940fe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b40147-7d92-4d5f-9d2a-2fb1d5c40441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b8345ca-f436-4c57-b5e5-bf8c93725801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b40147-7d92-4d5f-9d2a-2fb1d5c40441",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "461ba55e-7cb0-4ff5-a6c6-34ad16d187bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "cb7a5f24-5418-49b5-b252-8945f1d8febc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "461ba55e-7cb0-4ff5-a6c6-34ad16d187bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cab76ba-3b21-44cc-97a0-56f265105595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "461ba55e-7cb0-4ff5-a6c6-34ad16d187bc",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "fa079d32-0116-452d-8f00-ca6f4478751e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "bc499a80-ccbd-4909-80c9-dc2c12051e37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa079d32-0116-452d-8f00-ca6f4478751e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b703397e-5503-4cc7-997a-b24d97382e85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa079d32-0116-452d-8f00-ca6f4478751e",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "2265bc6a-ee04-4236-a644-9d2954e2950b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "19d732c4-0413-4f08-bb22-a3525add7fac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2265bc6a-ee04-4236-a644-9d2954e2950b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c94b80a0-8064-42c1-968b-55e57b4b6047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2265bc6a-ee04-4236-a644-9d2954e2950b",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "ef2bbd44-0def-4dcb-9200-b5f0c6068d38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "251b04e6-2821-4cee-8ebc-29983aa607af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef2bbd44-0def-4dcb-9200-b5f0c6068d38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d59e16de-1df0-479d-8884-5fa5fa764add",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef2bbd44-0def-4dcb-9200-b5f0c6068d38",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "99ee3052-ad32-4e79-a3c2-da9a417afeac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "cf7d6116-fee3-49d5-ac05-c302e44781c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99ee3052-ad32-4e79-a3c2-da9a417afeac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efe91e09-6e5a-4800-841f-eccfdb010166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ee3052-ad32-4e79-a3c2-da9a417afeac",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "f174f637-786a-4b7d-b453-e314cb5785f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "8b8421d1-79d7-4277-90c8-8c8c6572ea35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f174f637-786a-4b7d-b453-e314cb5785f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edc03db5-73ba-453f-aab7-1f974da10227",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f174f637-786a-4b7d-b453-e314cb5785f5",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "bca6e99c-9f52-4ad5-8a11-83c3d0e24fa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "2bd1eee2-89ca-4af2-9553-e9adc16d8deb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca6e99c-9f52-4ad5-8a11-83c3d0e24fa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c84f279-0c5d-4cb7-afe4-e37307e67b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca6e99c-9f52-4ad5-8a11-83c3d0e24fa1",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "5e2ae950-bcd0-4c95-9415-29e4291469af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "58309fc2-0ded-4d9f-8291-968c9b1d2fd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e2ae950-bcd0-4c95-9415-29e4291469af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0c9d655-821c-4be4-96bf-8fbbd8e6b69c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e2ae950-bcd0-4c95-9415-29e4291469af",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "2eb6765f-85a7-448a-9112-99c995e5b887",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "57bd91e2-61bb-4a15-9212-af9c8b4a3b3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2eb6765f-85a7-448a-9112-99c995e5b887",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb1f726c-257e-4fa7-b090-a39d8cdb8bab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2eb6765f-85a7-448a-9112-99c995e5b887",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "59a0bbe8-50a2-4e5d-a46f-5c68e1812e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "31753aca-3bec-4137-9b00-66820fa24b02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a0bbe8-50a2-4e5d-a46f-5c68e1812e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e60e28b-e72f-4166-a5cc-902964cf9bd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a0bbe8-50a2-4e5d-a46f-5c68e1812e1e",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "58b8902a-d48d-4929-9dff-7353dd4be727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "5bc44088-7ae1-45cb-af09-2aa39b9e1eb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58b8902a-d48d-4929-9dff-7353dd4be727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e18d459a-650c-41e7-a59a-5f371f0e0f33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58b8902a-d48d-4929-9dff-7353dd4be727",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "f94c06ba-4abc-492c-8141-c7c990224f46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "e4101862-8f1d-4a81-bc2a-841e39eba363",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f94c06ba-4abc-492c-8141-c7c990224f46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f82e489-e6ab-4d4b-b447-246e9b8cef72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f94c06ba-4abc-492c-8141-c7c990224f46",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        },
        {
            "id": "988a6bdc-f160-4293-b336-b9a7f654d5da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "compositeImage": {
                "id": "0d52b830-7572-473e-834f-b35c4b22a376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "988a6bdc-f160-4293-b336-b9a7f654d5da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b6d7c0-0592-4453-abd7-a623fe9da70b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "988a6bdc-f160-4293-b336-b9a7f654d5da",
                    "LayerId": "819b483a-4bc4-4e2a-b400-e7e7e08b8548"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "819b483a-4bc4-4e2a-b400-e7e7e08b8548",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e78b8e6-7899-4b82-84ed-4de58d4a34d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 192,
    "yorig": 96
}