{
    "id": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wolfWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 167,
    "bbox_left": 8,
    "bbox_right": 191,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f0a37a7-5d0b-4869-ab5c-ba1382b5b841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "d245eeaa-6af0-4b12-b098-dc07fa5c1487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f0a37a7-5d0b-4869-ab5c-ba1382b5b841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9afdb91-bbd4-4bb7-9568-25c140a4ccfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f0a37a7-5d0b-4869-ab5c-ba1382b5b841",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "b3f2ed2d-eb6c-4483-ab2f-e02872d15daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "3fa90916-c915-427e-942b-959600ae29e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3f2ed2d-eb6c-4483-ab2f-e02872d15daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9467fd81-9208-485a-ac3e-3805e2a3e6e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3f2ed2d-eb6c-4483-ab2f-e02872d15daf",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "4e39d7e0-1f23-4bbc-bdda-e40b22e6cfb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "0a632a23-106a-4d9d-8ca3-08514665ce5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e39d7e0-1f23-4bbc-bdda-e40b22e6cfb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "217ffdaa-7bf4-49d5-9ce5-c42b0fb3bdc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e39d7e0-1f23-4bbc-bdda-e40b22e6cfb9",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "d0f2a559-a717-4861-b9a2-1962be92b5a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "2ec9eb9c-a07b-4f6e-8f97-74360f6c1be0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0f2a559-a717-4861-b9a2-1962be92b5a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b5b1ec7-dd2f-4493-a69d-4f256810a701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0f2a559-a717-4861-b9a2-1962be92b5a3",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "1724c040-c5af-4b60-806c-2ae037dc34c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "02ce0828-3eb4-4b58-9c96-22f7afb5fdd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1724c040-c5af-4b60-806c-2ae037dc34c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1709421-8c39-4c19-be32-eab5d944dfee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1724c040-c5af-4b60-806c-2ae037dc34c2",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "53987412-fb65-4f06-b272-dc9834996d49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "4bdc3d94-b7ac-4b40-be9a-ccee0a0e7349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53987412-fb65-4f06-b272-dc9834996d49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6bc85c-844b-4e49-8cab-97102f81100d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53987412-fb65-4f06-b272-dc9834996d49",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "7329ba76-2e25-4354-bf14-d5336d4c7b88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "73d7e1fa-086f-41fa-a00e-8628fefe855a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7329ba76-2e25-4354-bf14-d5336d4c7b88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4185e48-4fdf-4d73-ab25-e388ce561a08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7329ba76-2e25-4354-bf14-d5336d4c7b88",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "84b1bd93-09e9-4473-92f9-a09bd41a3b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "6b8069df-cba7-4c2d-8e4d-8a6670272f45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b1bd93-09e9-4473-92f9-a09bd41a3b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a082a4a5-65e9-4e99-94a1-73095233da0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b1bd93-09e9-4473-92f9-a09bd41a3b5c",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "bd8f6e9f-f4b8-48ab-a642-a368b8ff85cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "a91251ea-d67f-4261-99fc-1489254216b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd8f6e9f-f4b8-48ab-a642-a368b8ff85cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7872eae0-1183-4acf-baf8-deb0dcf2da91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd8f6e9f-f4b8-48ab-a642-a368b8ff85cc",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "83e0553e-2d2f-4b23-baff-4f61fe3d8b80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "8916e42f-7028-453d-9898-167fdcc52681",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83e0553e-2d2f-4b23-baff-4f61fe3d8b80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "830fb1a3-f1aa-4bc3-bcdd-3e7e590a5c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83e0553e-2d2f-4b23-baff-4f61fe3d8b80",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "c75b02f4-1496-49e0-8e48-4f8c4e7a0816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "d4ab0b62-aa7b-4840-b612-33f097077a7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c75b02f4-1496-49e0-8e48-4f8c4e7a0816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f3cf44-85bc-4474-ad78-1d79cb8b9dac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c75b02f4-1496-49e0-8e48-4f8c4e7a0816",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        },
        {
            "id": "e3130407-e97d-4886-9040-7374404fc12c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "compositeImage": {
                "id": "7adc535f-f53b-45b4-b4e7-f40b0c32140a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3130407-e97d-4886-9040-7374404fc12c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4e9dee8-0848-4bc8-aa25-5157b0926eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3130407-e97d-4886-9040-7374404fc12c",
                    "LayerId": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "9b719a7b-e3cf-4442-9f02-8b6892a1c2e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75ceaf60-20bd-437b-8f49-34d51bf93dc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}