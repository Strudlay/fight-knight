{
    "id": "81b2fb25-8263-462f-a95c-e57d01cb0f8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gunnerPortrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 0,
    "bbox_right": 154,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14be1deb-b55c-41ce-9f2b-845bf95bc03f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81b2fb25-8263-462f-a95c-e57d01cb0f8d",
            "compositeImage": {
                "id": "157613fa-1bff-4a99-b997-1e26f2bd4be8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14be1deb-b55c-41ce-9f2b-845bf95bc03f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c4c7761-064c-4bc7-9653-69b6007785e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14be1deb-b55c-41ce-9f2b-845bf95bc03f",
                    "LayerId": "b8e98360-f306-4bed-8e9c-fd46a65a1cf2"
                }
            ]
        },
        {
            "id": "876d8690-7404-4767-a365-16e03a2c2771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81b2fb25-8263-462f-a95c-e57d01cb0f8d",
            "compositeImage": {
                "id": "c6f8dc0a-c2fc-4bea-a5fd-5c3062686327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "876d8690-7404-4767-a365-16e03a2c2771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b665c64d-2696-4c05-b67c-134db3302f9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "876d8690-7404-4767-a365-16e03a2c2771",
                    "LayerId": "b8e98360-f306-4bed-8e9c-fd46a65a1cf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "b8e98360-f306-4bed-8e9c-fd46a65a1cf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81b2fb25-8263-462f-a95c-e57d01cb0f8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 155,
    "xorig": 77,
    "yorig": 58
}