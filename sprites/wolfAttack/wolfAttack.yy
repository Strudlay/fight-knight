{
    "id": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wolfAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 183,
    "bbox_left": 0,
    "bbox_right": 279,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c35a582-21f0-4364-be99-59d5a7aa47d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "836188f7-0d55-449c-90e9-35df08b92f0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c35a582-21f0-4364-be99-59d5a7aa47d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc72f84f-904c-4cf4-8ddc-1b89da677cf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c35a582-21f0-4364-be99-59d5a7aa47d5",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "3d0b2973-0951-44a4-894e-be1642670b76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "df619290-097b-469a-a934-0c8beaaf3317",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d0b2973-0951-44a4-894e-be1642670b76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bac46cf-88a6-480a-950b-19a1553b6ff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d0b2973-0951-44a4-894e-be1642670b76",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "77c0d40c-492d-409d-b631-5afb18dba349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "d8d7e40b-4f17-47c2-8819-957158f9ca1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77c0d40c-492d-409d-b631-5afb18dba349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb2bd110-5d8e-4c72-b2ab-7a56850a374e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77c0d40c-492d-409d-b631-5afb18dba349",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "4e569df4-6402-4e41-a5c5-b70764b5e32f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "bb136dce-9477-4075-a73c-1ae1f1a0ae3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e569df4-6402-4e41-a5c5-b70764b5e32f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f30c1f98-a3a1-484f-8e20-107c05ed72be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e569df4-6402-4e41-a5c5-b70764b5e32f",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "02415a4c-6165-40a4-a473-b39fda5aa283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "6d665ec7-3f64-47a0-86f4-6f7a000abc79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02415a4c-6165-40a4-a473-b39fda5aa283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c362f05-9b93-4dab-a296-28f14c3a74b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02415a4c-6165-40a4-a473-b39fda5aa283",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "ebbd1f43-e0dd-4037-a09c-5509e0cd2b8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "136798eb-9acd-418a-b5b6-339c2e6c51cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebbd1f43-e0dd-4037-a09c-5509e0cd2b8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1a79c8e-010e-44b8-8ecf-27e620f32031",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebbd1f43-e0dd-4037-a09c-5509e0cd2b8f",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "ed2f49cd-5d12-43b2-a404-c8426aa071b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "e374414e-cda6-486c-a817-10c0ef254d26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed2f49cd-5d12-43b2-a404-c8426aa071b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f0dce42-6ee8-4b55-93c1-9a81d639af63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed2f49cd-5d12-43b2-a404-c8426aa071b0",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "ef458b5c-8cdd-4afb-b499-6fd451b799e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "78e994bd-5b8e-4f5f-b7e7-a94a78b4caa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef458b5c-8cdd-4afb-b499-6fd451b799e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2732f66-bc7c-4af6-bdd8-d72cb733e47f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef458b5c-8cdd-4afb-b499-6fd451b799e6",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "a031f615-1eb2-4cb2-af53-e6d87e4a7717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "338949e5-caae-425b-a4ae-e2f8a2f1082f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a031f615-1eb2-4cb2-af53-e6d87e4a7717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fcb8579-2b75-4f06-9cc2-91c356d262df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a031f615-1eb2-4cb2-af53-e6d87e4a7717",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "07087dd3-af28-4109-b466-385cce8e49d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "8fb328ad-9624-42e8-b34f-d817b1bc32e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07087dd3-af28-4109-b466-385cce8e49d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a18c35c-3718-46ef-ab3f-de0862cd6fd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07087dd3-af28-4109-b466-385cce8e49d3",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "c70bb12f-2d87-4448-aebb-a6c17db90080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "8bc56d10-abeb-4167-8582-4949889feb30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70bb12f-2d87-4448-aebb-a6c17db90080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "550a754d-59ae-4108-934f-9a4244c2c6b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70bb12f-2d87-4448-aebb-a6c17db90080",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "ce408e67-b0a0-4b04-8fc3-bcae3e3227e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "e4055e3d-bc90-4faa-a82c-ff8d8d8fed6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce408e67-b0a0-4b04-8fc3-bcae3e3227e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "492892a3-0d9b-4979-8031-56d717588106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce408e67-b0a0-4b04-8fc3-bcae3e3227e9",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "e4862885-e9c7-47f1-bec0-ff64c26cab56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "cbfb34c3-8d3c-4a57-9fec-cc942d6bad8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4862885-e9c7-47f1-bec0-ff64c26cab56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "955496c0-87c8-48e6-b3af-96876d531db4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4862885-e9c7-47f1-bec0-ff64c26cab56",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "ed685252-a14b-4c65-84a5-56bc0d82ec36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "3f02351c-0394-4895-8737-a92ce4470f03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed685252-a14b-4c65-84a5-56bc0d82ec36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82260b90-37a4-416d-b7cf-3602fe6b13a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed685252-a14b-4c65-84a5-56bc0d82ec36",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        },
        {
            "id": "5cc857df-c7fe-47c7-a0ea-a42b9a4a429a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "compositeImage": {
                "id": "5225fc67-c583-4eb8-9711-eb7d6f5443bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cc857df-c7fe-47c7-a0ea-a42b9a4a429a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db1ea816-eb2c-4975-b634-9ad2b2269323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cc857df-c7fe-47c7-a0ea-a42b9a4a429a",
                    "LayerId": "ef2626fc-79c0-4ede-bc69-e9e03ff39179"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "ef2626fc-79c0-4ede-bc69-e9e03ff39179",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08cb3f6f-f207-4ca8-9a6d-ae4d38fbf940",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 144,
    "yorig": 96
}