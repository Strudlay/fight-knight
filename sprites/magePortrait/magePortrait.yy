{
    "id": "532fd3c1-d9bc-4f26-921b-3c4e5a1bf50c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "magePortrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 20,
    "bbox_right": 135,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30f7f793-9398-43cc-bc49-7b1293a418dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "532fd3c1-d9bc-4f26-921b-3c4e5a1bf50c",
            "compositeImage": {
                "id": "8ea2bd28-13aa-4768-ac45-fd02fa2019ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30f7f793-9398-43cc-bc49-7b1293a418dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96924e71-b1e6-4565-be66-7903eb0dbfa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30f7f793-9398-43cc-bc49-7b1293a418dd",
                    "LayerId": "dc25ba5f-62ec-494f-b541-1a708a8a8388"
                }
            ]
        },
        {
            "id": "ff2c280c-f601-4d98-a583-5d6082b32722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "532fd3c1-d9bc-4f26-921b-3c4e5a1bf50c",
            "compositeImage": {
                "id": "92b2564d-b8a1-41c6-9bcc-7dcee5c688ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff2c280c-f601-4d98-a583-5d6082b32722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf038f7-c345-44ff-9935-333fb0340764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff2c280c-f601-4d98-a583-5d6082b32722",
                    "LayerId": "dc25ba5f-62ec-494f-b541-1a708a8a8388"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "dc25ba5f-62ec-494f-b541-1a708a8a8388",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "532fd3c1-d9bc-4f26-921b-3c4e5a1bf50c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 155,
    "xorig": 85,
    "yorig": 58
}