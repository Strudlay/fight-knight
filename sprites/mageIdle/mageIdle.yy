{
    "id": "3de1100e-e45c-457d-b84e-cfbd3968a194",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mageIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 60,
    "bbox_right": 115,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c7d5395-22a4-4221-a278-bcc500e2a236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "compositeImage": {
                "id": "a26cdc19-2c28-48cd-ab1c-534223e8314b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c7d5395-22a4-4221-a278-bcc500e2a236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27f8c24e-5602-480d-b4b7-c31e9265a244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c7d5395-22a4-4221-a278-bcc500e2a236",
                    "LayerId": "11428312-d52e-49a3-9de5-d3f4b4f55274"
                }
            ]
        },
        {
            "id": "cb751bca-80b4-49fe-b167-4c2ff61bde0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "compositeImage": {
                "id": "f7d73b8b-d20d-4ebc-b7fb-a59c067673fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb751bca-80b4-49fe-b167-4c2ff61bde0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ea3e8e2-a7dd-42e5-9ead-766f0be0ac89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb751bca-80b4-49fe-b167-4c2ff61bde0b",
                    "LayerId": "11428312-d52e-49a3-9de5-d3f4b4f55274"
                }
            ]
        },
        {
            "id": "ed85df71-23d5-42ea-ba62-f17598200102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "compositeImage": {
                "id": "6a3166e7-a8b0-4e50-ab8a-47a229aee03f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed85df71-23d5-42ea-ba62-f17598200102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab33ef0c-b207-42f2-92b5-e1319ef91f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed85df71-23d5-42ea-ba62-f17598200102",
                    "LayerId": "11428312-d52e-49a3-9de5-d3f4b4f55274"
                }
            ]
        },
        {
            "id": "aa03df5b-d4f5-49fa-8d53-8976ba4c5386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "compositeImage": {
                "id": "f2c1d995-bfd5-4f1a-a820-7006d61debab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa03df5b-d4f5-49fa-8d53-8976ba4c5386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acb5a175-a098-41db-8d8c-5978765db032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa03df5b-d4f5-49fa-8d53-8976ba4c5386",
                    "LayerId": "11428312-d52e-49a3-9de5-d3f4b4f55274"
                }
            ]
        },
        {
            "id": "ab8020be-5c57-499d-955e-faab7cc5a785",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "compositeImage": {
                "id": "9066c250-9c62-42fd-838f-c22a37671ad5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab8020be-5c57-499d-955e-faab7cc5a785",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2b0f865-946e-4500-99a5-b554713b2451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab8020be-5c57-499d-955e-faab7cc5a785",
                    "LayerId": "11428312-d52e-49a3-9de5-d3f4b4f55274"
                }
            ]
        },
        {
            "id": "b8906ed5-7e39-4b3c-ae52-f391af99d6f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "compositeImage": {
                "id": "82ac0ff3-a52a-4745-903f-0b247b4c5bbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8906ed5-7e39-4b3c-ae52-f391af99d6f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47927c69-0558-4531-89f1-1d154b8ed66f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8906ed5-7e39-4b3c-ae52-f391af99d6f1",
                    "LayerId": "11428312-d52e-49a3-9de5-d3f4b4f55274"
                }
            ]
        },
        {
            "id": "04f0d136-9728-4fc4-b8bf-4e679d2fd37a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "compositeImage": {
                "id": "a689765d-f5e5-49c4-b79e-c376b308b48d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f0d136-9728-4fc4-b8bf-4e679d2fd37a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d79cd926-66cc-4e36-b4c6-af0a4c84f1e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f0d136-9728-4fc4-b8bf-4e679d2fd37a",
                    "LayerId": "11428312-d52e-49a3-9de5-d3f4b4f55274"
                }
            ]
        },
        {
            "id": "0c83630a-7e07-4f68-b7ce-b909b21d6e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "compositeImage": {
                "id": "6fd14272-eb19-4dff-9e60-40705cbf3f36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c83630a-7e07-4f68-b7ce-b909b21d6e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ddb67b9-c108-4850-8360-2a089b20dca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c83630a-7e07-4f68-b7ce-b909b21d6e65",
                    "LayerId": "11428312-d52e-49a3-9de5-d3f4b4f55274"
                }
            ]
        },
        {
            "id": "e24b8e18-28b7-42e3-b87b-c596b8cff7a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "compositeImage": {
                "id": "c7445897-5c69-474d-b2f3-0dbf5bd0b35f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e24b8e18-28b7-42e3-b87b-c596b8cff7a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0ad4dd2-dbd1-47a5-8647-d4bbf510dcbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e24b8e18-28b7-42e3-b87b-c596b8cff7a4",
                    "LayerId": "11428312-d52e-49a3-9de5-d3f4b4f55274"
                }
            ]
        },
        {
            "id": "87216af5-38cb-480d-8988-5a83ab62c7c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "compositeImage": {
                "id": "978f81e4-c529-4ba5-9655-5e95d683fd14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87216af5-38cb-480d-8988-5a83ab62c7c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3cb8fc4-0345-4ef1-bf33-4b091bc5dd69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87216af5-38cb-480d-8988-5a83ab62c7c1",
                    "LayerId": "11428312-d52e-49a3-9de5-d3f4b4f55274"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "11428312-d52e-49a3-9de5-d3f4b4f55274",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3de1100e-e45c-457d-b84e-cfbd3968a194",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}