{
    "id": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gunnerWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 16,
    "bbox_right": 131,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4317a040-c5c5-4493-ac70-8f4ee9d8399a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "e67f94d2-785b-45a2-a3fa-c9ca4d756263",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4317a040-c5c5-4493-ac70-8f4ee9d8399a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52d72b9e-f0d5-4a34-8d30-f85d176af4c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4317a040-c5c5-4493-ac70-8f4ee9d8399a",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "493bf4b1-1e8a-4d24-9e19-8c9d94d6c65a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "49ae83f4-3386-4ed9-b8a6-1751abf1457a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "493bf4b1-1e8a-4d24-9e19-8c9d94d6c65a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e1c557-db05-4af2-bf5b-35b2946748de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "493bf4b1-1e8a-4d24-9e19-8c9d94d6c65a",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "4023bd09-9782-42c2-9a19-e043e0070951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "02899286-f6fc-4554-937b-3b0eed0007c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4023bd09-9782-42c2-9a19-e043e0070951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab7d88eb-eb59-4908-887e-03d0290e4cb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4023bd09-9782-42c2-9a19-e043e0070951",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "d5a63283-abf3-42df-b67e-8b307ab6dc0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "3f7532aa-aa72-476f-9538-4c5248ddbe0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a63283-abf3-42df-b67e-8b307ab6dc0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "026b7fcf-da1b-478a-957a-c803f3f6aa07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a63283-abf3-42df-b67e-8b307ab6dc0a",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "d598c563-f515-4018-b050-3082d70f1a91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "01a671f3-f0b0-48b8-8e84-031c75709032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d598c563-f515-4018-b050-3082d70f1a91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "661f0072-846a-49e2-9732-5c4b846e3668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d598c563-f515-4018-b050-3082d70f1a91",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "e1f86f23-97ee-418b-86a5-f0e1bb074d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "9ce4cd6b-e30a-4de4-b441-b4d12f300efe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1f86f23-97ee-418b-86a5-f0e1bb074d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2265b13-f552-474c-a7c2-eabf34887553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f86f23-97ee-418b-86a5-f0e1bb074d1a",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "1bd9ae35-1221-4032-a24d-6d55a35f8845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "918c1daf-6a38-4116-8dd4-f1d5a4010c3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bd9ae35-1221-4032-a24d-6d55a35f8845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a1e7a32-0e35-436b-bdeb-f466e6c5fb3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bd9ae35-1221-4032-a24d-6d55a35f8845",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "0ce8eac4-730b-4040-a160-a2d6c0c15ef2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "258c3077-62bf-457f-bd73-3f6b8ed8ae15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ce8eac4-730b-4040-a160-a2d6c0c15ef2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43161fd3-28e8-4eee-b689-2887a07e7fd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ce8eac4-730b-4040-a160-a2d6c0c15ef2",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "f0efc740-15fb-4672-9ae7-672a958f63bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "0181e4c4-0359-441a-8cbe-ce453adb4baa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0efc740-15fb-4672-9ae7-672a958f63bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eff40da-a14e-41e7-8ee5-154ab5bd8c5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0efc740-15fb-4672-9ae7-672a958f63bf",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "56086feb-8e8d-466c-9a23-5932ca65bb2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "d6dd67c7-541f-4eba-9d8a-73d46062f000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56086feb-8e8d-466c-9a23-5932ca65bb2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa783ec8-c382-4566-8a93-c21a63a22fef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56086feb-8e8d-466c-9a23-5932ca65bb2c",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "94f10248-9e5c-4423-afc3-ff82cb3d08b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "0e1db4d5-071b-4d1b-875f-92bb290c3702",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94f10248-9e5c-4423-afc3-ff82cb3d08b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee2e1b69-483c-4707-aacc-3220d16a9d28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94f10248-9e5c-4423-afc3-ff82cb3d08b7",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "fc30a13c-2fd3-46e2-b3e2-5ab15e58b607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "86186150-34a6-4b6c-9fcd-a7c2772ddee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc30a13c-2fd3-46e2-b3e2-5ab15e58b607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "870369b5-d046-435d-88d2-9828e605a5d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc30a13c-2fd3-46e2-b3e2-5ab15e58b607",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "12531bb7-2db3-41e6-967f-6c78378d7aba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "320a4c0d-0e83-4860-820a-c3ef93f7735e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12531bb7-2db3-41e6-967f-6c78378d7aba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a00290f9-3f40-4f00-a7ee-129534a9a64c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12531bb7-2db3-41e6-967f-6c78378d7aba",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        },
        {
            "id": "35bd142c-8c88-4edd-941a-8675eb6e07ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "compositeImage": {
                "id": "1a52db51-a8bd-4efc-a4af-678106b23941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35bd142c-8c88-4edd-941a-8675eb6e07ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1b0d673-005d-411f-b260-40f7e4cdd87e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35bd142c-8c88-4edd-941a-8675eb6e07ff",
                    "LayerId": "b57a58f5-2254-47bc-965b-6793712bc6a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "b57a58f5-2254-47bc-965b-6793712bc6a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5950f7f5-02a3-4799-b79a-368e52aec1ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}